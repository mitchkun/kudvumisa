<?php
ini_set('display_errors',0);
require "includes/config.php";
require "includes/reception.php";
if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
    if ($_SESSION['success'] == 'Forwarded to Nurse Department') {
        header("Refresh:5; url=newreception.php");
    }
}
include('includes/header.php');
include('includes/navbar.php');

?>

<div class="container-fluid">
    <?php
    if (isset($_REQUEST['searchit'])) {
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $searchq = $_REQUEST['searchit'];
        $query = "SELECT * FROM clients WHERE " . $_REQUEST['criteria'] . " LIKE '%$searchq%'  ORDER BY `clients`.`Surname` ASC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    } else if (isset($_REQUEST['emr_no'])) {
        $emr_no = $_REQUEST['emr_no'];
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query = "SELECT * FROM clients WHERE `emr_no` = $emr_no ORDER BY `clients`.`emr_no`  DESC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    } else {
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query = "SELECT * FROM clients ORDER BY `clients`.`emr_no`  DESC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    }

    if (mysqli_num_rows($query_run) > 0) {
        $row = mysqli_fetch_assoc($query_run)



    ?>

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Client Profile</h1>
            <a href="reception.php?start=<?php if (isset($_REQUEST['searchit'])) {
                                                echo ($start == 0) ? 0 : $start - 1 . "&searchit=" . $_REQUEST['searchit'] . "&criteria=" . $_REQUEST['criteria'];
                                            } else {
                                                echo ($start == 0) ? 0 : $start - 1;
                                            } ?>" class="previous">&laquo; Previous</a>
            <a href="reception.php?<?php if (isset($_REQUEST['searchit'])) {
                                        $start++;
                                        echo "searchit=" . $_REQUEST['searchit'] . "&criteria=" . $_REQUEST['criteria'] . "&start=" . $start;
                                    } else {
                                        $start++;
                                        echo "start=" . $start;
                                    } ?>" class="next">Next &raquo;</a>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="...">
                                <h5 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h5>
                            </a>
                            <p class="description text-center">
                                EMR #: <?php echo $row['emr_no']; ?>
                            </p>
                        </div>

                    </div>
                    <div class="card-footer">
                        <?php
                        $emr1 = $row['emr_no'];
                        $queryvitals1 = "SELECT * FROM vitals WHERE emr_no = $emr1 ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                        $query_run_vitals1 = mysqli_query($conn, $queryvitals1);
                        if (mysqli_num_rows($query_run_vitals1) > 0) {
                            $row_vitals1 = mysqli_fetch_assoc($query_run_vitals1);
                            $timefromdatabase = strtotime($row_vitals1['date_taken']);

                            $dif = time() - $timefromdatabase;
                            if ($dif < 86400) {
                        ?>

                                <h5 class="title text-center">Vitals (<?php echo $row_vitals1['date_taken']; ?>)</h5>
                        <?php
                            } else {
                                echo "<h5 class='title text-center'>Vitals </h5>";
                            }
                        } else {
                            echo "<h5 class='title text-center'>Vitals </h5>";
                        }

                        ?>
                        <hr>
                        <div class="button-container">
                            <?php
                            $emr = $row['emr_no'];
                            $queryvitals = "SELECT * FROM vitals WHERE emr_no = $emr ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                            $query_run_vitals = mysqli_query($conn, $queryvitals);
                            if (mysqli_num_rows($query_run_vitals) > 0) {
                                $row_vitals = mysqli_fetch_assoc($query_run_vitals);
                                $timefromdatabase = strtotime($row_vitals['date_taken']);

                                $dif = time() - $timefromdatabase;
                                if ($dif < 86400) {
                            ?>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['wt']; ?>Kgs
                                                <br>
                                                <small>Weight</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo $row_vitals['temp']; ?> &#8451;
                                                <br>
                                                <small>Temparature</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6><?php echo $row_vitals['bp_sys']; ?> / <?php echo $row_vitals['bp_dia']; ?>
                                                <br>
                                                <small>BP</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['height']; ?> cm
                                                <br>
                                                <small>Height</small>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo round($row_vitals['pulse']); ?> bpm
                                                <br>
                                                <small>Pulse</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo round($row_vitals['rr']); ?> bpm
                                                <br>
                                                <small>RR</small>
                                            </h6>
                                        </div>
                                    </div>
                            <?php
                                } else {
                                    echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                                }
                            } else {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                            }

                            ?>
                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Forward to Department</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#LabModal'>Lab</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#HtsModal'>HTS</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#NursesModal'>Nurse</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PharmModal'>Pharmacy</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card card-user">
                    <div class="card-header">
                        <?php

                        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                            echo '<h2>' . $_SESSION['success'] . '</h2>';

                            unset($_SESSION['success']);
                        }

                        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                            echo '<h2>' . $_SESSION['status'] . '</h2>';
                            unset($_SESSION['status']);
                        }


                        ?>
                        <h5 class="card-title">Edit Profile</h5>
                    </div>
                    <div class="card-body">
                        <form action="updateprofile.php?emr_no=<?php echo $emr . "&"; ?>" method="POST">
                            <div class="row">
                                <div class="col-md-5 pr-1">
                                    <div class="form-group">
                                        <label>National ID Number/PIN</label>
                                        <input type="text" id="ID" name="ID" class="form-control" disabled="" value="<?php echo $row['ID']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label>Surname</label>
                                        <input type="text" id="Surname" name="Surname" class="form-control" value="<?php echo $row['Surname']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">First Name</label>
                                        <input type="text" id="Name" name="Name" class="form-control" value="<?php echo $row['Name']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>DOB</label>
                                        <input type="date" id="DOB" name="DOB" class="form-control" value="<?php echo $row['DOB']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Sex</label><br />
                                        <?php
                                        if (isset($row['Gender'])) {
                                            if ($row['Gender'] == "Female") {
                                                echo "
                                                <select id='Gender' name='Gender' class='form-control'>
                                                <option id='male' value='Male'>Male</option>                                            
                                                <option id='female' value='Female' selected='true'>Female</option>
                                                </select> " . $row['Gender'];
                                            } else if ($row['Gender'] == "Male") {
                                                echo "<select id='Gender' name='Gender' selected='true' class='form-control'>
                                                <option id='male' value='Male'>Male</option>                                            
                                                <option id='female' value='Female' >Female</option>
                                                </select> " . $row['Gender'];
                                            } else {
                                                echo "<select id='Gender' name='Gender' class='form-control'>
                                                <option id='male' value='Male'>Male</option>                                            
                                                <option id='female' value='Female'>Female</option>
                                                </select>";
                                            }
                                        } else {
                                            echo "<select id='Gender' name='Gender' class='form-control'>
                                            <option id='male' value='Male'>Male</option>                                            
                                            <option id='female' value='Female'>Female</option>
                                            </select>";
                                        } ?>
                                        <!-- <input type="radio" id="male" name="gender" value="male">
                                        <label for="male">Male</label>
                                        <input type="radio" id="female" name="gender" value="female" checked="true">
                                        <label for="female">Female</label> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Physical Address (Including how to reach the home)</label>
                                        <input type="text" id="address" name="address" class="form-control" value="<?php echo $row['address']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Tel/Cell</label>
                                        <input type="text" id="Contact" name="Contact" class="form-control" value="<?php echo $row['Contact']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Occupation</label>
                                        <input type="text" id="Occupation" name="Occupation" class="form-control" value="<?php echo $row['Occupation']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Telephone Work</label>
                                        <input type="number" id="work_contact" name="work_contact" class="form-control" value="<?php echo $row['work_contact']; ?>">
                                        <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                                echo $nstart; ?>" hidden="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Marital Status</label><br />
                                        <?php
                                        if (isset($row['Marital'])) {
                                            if ($row['Marital'] == "Single") {
                                                echo "<select id='married' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' selected='true'>Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Married") {
                                                echo "<select id='married' name='Marital' class='form-control'>
                                                <option value='Married' selected='true'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Widowed") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' Single</option>
                                               
                                                <option value='Widowed' selected='true'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Divorced") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced' selected='true'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Co-habitation") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation' selected='true'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> ";
                                            }
                                        } else {
                                            echo "<select id='Marital' name='Marital' class='form-control'>
                                            <option value='Married'>Married</option>
                                           
                                            <option value='Single' >Single</option>
                                           
                                            <option value='Widowed'>Widowed</option>
                                           
                                            <option value='Divorced'>Divorced</option>
                                           
                                            <option value='Co-habitation'>Co-habitation</option>
                                            </select> ";
                                        } ?>
                                        <!-- <input type="radio" id="married" name="marital" value="married">
                                        <label for="married">Married</label>
                                        <input type="radio" id="single" name="marital" value="single">
                                        <label for="single">Single</label>
                                        <input type="radio" id="widowed" name="marital" value="widowed">
                                        <label for="widowed">Widowed</label>
                                        <input type="radio" id="divorced" name="marital" value="divorced">
                                        <label for="divorced">Divorced</label> -->
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Orphan</label>
                                        <br />
                                        <?php
                                        if (isset($row['Orphan'])) {
                                            if ($row['Orphan'] == "Yes") {
                                                echo  "<select name='Orphan' id='Orphan' class='form-control'>
                                                <option value='Yes' selected='true'>Yes</option>
                                                <option value='No'>No</option>
                                                </select> " . $row['Orphan'];
                                            } else if ($row['Orphan'] == "No") {
                                                echo "<select name='Orphan' id='Orphan'>
                                                <option value='Yes'>Yes</option>
                                                <option value='No' selected='true'>No</option>
                                                </select> " . $row['Orphan'];
                                            } else {
                                                echo "<select name='Orphan' id='Orphan' class='form-control'>
                                                <option value='Yes'>Yes</option>
                                                <option value='No'>No</option>
                                                </select> ";
                                            }
                                        } else {
                                            echo "<select name='Orphan' id='Orphan' class='form-control'>
                                            <option value='Yes'>Yes</option>
                                            <option value='No'>No</option>
                                            </select> ";
                                        } ?>
                                        <!--                                         
                                        <input type="radio" id="yes" name="orphan" value="yes">
                                        <label for="yes">Yes</label>
                                        <input type="radio" id="yes" name="orphan" value="yes">
                                        <label for="no">No</label> -->
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Living Parent (tick appropriate)</label>
                                        <br><?php if (isset($row['Parent'])) echo $row['Parent']; ?><br>
                                        <?php
                                        // if (isset($row['Parent'])) {
                                        //     if ($row['Parent'] == "Mother") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother' selected='true'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else if ($row['Parent'] == "Father") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father' selected='true'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else if ($row['Parent'] == "None") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None' selected='true'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>";
                                        //     }
                                        // } else {
                                        //     echo   "<select name='Parent' id='Parent'>
                                        //     <option value='Mother'>Mother</option>
                                        //     <option value='Father'>Father</option>
                                        //     <option value='None'>None</option>
                                        //     </select>";
                                        //} 
                                        ?>
                                        <input type="checkbox" id="mother" name="mother" value="mother">
                                        <label for="mother">Mother</label>
                                        <input type="checkbox" id="father" name="father" value="father">
                                        <label for="father">Father</label>
                                        <input type="checkbox" id="none" name="none" value="none">
                                        <label for="none">None</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Religion</label>
                                        <input type="text" id="Religion" name="Religion" class="form-control" value="<?php echo $row['Religion']; ?>">
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Occupation</label>
                                    <input type="text" class="form-control" placeholder="Country" value="Australia">
                                </div>
                            </div> -->
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Education Level</label>
                                        <input type="text" id="Education" name="Education" class="form-control" value="<?php echo $row['Education']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Nearest Clinic</label>
                                        <input type="text" class="form-control" name="nearest_clinic" value="<?php echo $row['nearest_clinic']; ?>">
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Occupation</label>
                                    <input type="text" class="form-control" placeholder="Country" value="Australia">
                                </div>
                            </div> -->
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Nearest School</label>
                                        <input type="text" class="form-control" name="nearest_school" value="<?php echo $row['nearest_school']; ?>">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" name="updatebtn" id="updatebtn" class="btn btn-primary btn-round">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    <?php

    } else {
        echo "<a href='newreception.php' >No records found</a>";
    }

    ?>
</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <!-- <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button> -->
                    <button id="bt-start" type="button" class="mgo-fingerprint-tg-start btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                                <input type="hidden" name="destination" value="reception.php"/>
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bpm" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... bpm" id="rr" name="rr">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="NursesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="newreception.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='0' checked>
                                <label for='working'>Regular</label>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="PharmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Pharmacy Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="newreception.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Pharm" id="vitals_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="LabModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Lab Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="newreception.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Lab" id="vitals_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="HtsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Hts Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="newreception.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Hts" id="Hts_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>