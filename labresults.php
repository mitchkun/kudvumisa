<?php

require "includes/config.php";
require "includes/lab.php";
include('includes/header.php');
include('includes/labtestnavbar.php');
?>

<div class="container-fluid">
    <?php



    ?>

    <div class="table-responsive">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        if (isset($_REQUEST['art_lab'])) {
            $query = "SELECT * FROM ccf_lab_test WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>ART Results</h2>";
        } else if (isset($_REQUEST['cd4_lab'])) {
            $query = "SELECT * FROM cd4_lab_test WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>CD4 Count Results</h2>";
        } else if (isset($_REQUEST['vl_lab'])) {
            $query = "SELECT * FROM ccf_viral_load_results WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>Viral Load Results</h2>";
        } else if (isset($_REQUEST['hiv_lab'])) {
            $query = "SELECT * FROM hiv_lab_test WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>HIV Results</h2>";
        } else if (isset($_REQUEST['tb_lab'])) {
            $query = "SELECT * FROM tb_lab_test WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>TB Results</h2>";
        }
         else {
            $query = "SELECT * FROM ccf_lab_test WHERE `status` = 'Pending' ORDER BY `date_drawn` DESC";
            echo "<h2>ART Results</h2>";
        }
        $query_run = mysqli_query($conn, $query);



        ?>

        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th> EMR # </th>
                    <th> Result Status </th>
                    <th>Date Drawn </th>
                    <th>Reason</th>

                    <?php

                    if (isset($_REQUEST['art_lab'])) {
                        echo "<th>AST Results </th><th>ALT Results </th><th>BUN Results </th><th>CREAT Results </th>";
                    } else if (isset($_REQUEST['cd4_lab'])) {
                        echo "<th>CD4 Results </th>";
                    } else if (isset($_REQUEST['vl_lab'])) {
                        echo "<th>Viral Load Results </th>";
                    }

                    ?>
                    <th>Save </th>
                </tr>
            </thead>
            <tbody>
                <?php

                if (mysqli_num_rows($query_run) > 0) {
                    while ($row = mysqli_fetch_assoc($query_run)) {
                ?>

                        <form action="functions/labresult.php" method="post">
                            <tr>
                                <td> <?php echo $row['emr_no']; ?> </td>
                                <td> <?php echo $row['status'] ?></td>
                                <td> <?php echo $row['Date_drawn']; ?></td>
                                <td><?php echo $row['reason']; ?> </td>
                                <?php

                                if (isset($_REQUEST['art_lab'])) {
                                    echo "<td>
                            <input type='text' name='ast_result' class='form-control' >
                            <input type='hidden' name='lab_id' value='" . $row['lab_id'] . "' >
                        </td>
                        <td>
                        <input type='text' name='alt_result' class='form-control' >
                        </td>
                        <td>
                        <input type='text' name='bun_result' class='form-control' >
                        </td>
                        <td>
                        <input type='text' name='creat_result' class='form-control' >
                        </td>
                        ";
                                } else if (isset($_REQUEST['cd4_lab'])) {
                                    echo "<td>
                                        <input type='text' name='cd4_result' class='form-control' >
                                        <input type='hidden' name='lab_id' value='" . $row['lab_id'] . "' >
                                    </td>";
                                } else if (isset($_REQUEST['vl_lab'])) {
                                    echo "<td>
                                        <input type='text' name='vl_result' class='form-control' >
                                        <input type='hidden' name='lab_id' value='" . $row['lab_id'] . "' >
                                    </td>";
                                }

                                ?>

                                <td>

                                    <button type="submit" name="<?php if (isset($_REQUEST['art_lab'])) {
                                                                    echo "art_lab";
                                                                } else if (isset($_REQUEST['cd4_lab'])) {
                                                                    echo "cd4_lab";
                                                                } else if (isset($_REQUEST['vl_lab'])) {
                                                                    echo "vl_lab";
                                                                } ?>" class="btn btn-success"> SAVE</button>

                                </td>
                            </tr>
                        </form>

                <?php

                    }
                } else echo "<tr>No Records</tr>";


                ?>

            </tbody>
        </table>

    </div>


</div>

</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bps" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="rr" name="rr">
                                <input type="hidden" name="destination" value="lab.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="NursesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="lab.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="DismissModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Dismiss Home</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Dismiss</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="lab.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="dismiss" id="dismiss" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>