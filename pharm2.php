<?php

require "includes/config.php";
require "includes/pharm.php";
include('includes/header.php');
include('includes/pharmnavbar.php');
?>

<div class="container-fluid">
    <?php
    if (isset($_REQUEST['searchit'])) {
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $searchq = $_REQUEST['searchit'];
        $query = "SELECT * FROM clients , `prescription` WHERE `prescription`.`emr_no` = `clients`.`emr_no` AND " . $_REQUEST['criteria'] . " LIKE '%$searchq%'  ORDER BY `clients`.`Surname` ASC limit $start, 1";
        $query_run = mysqli_query($conn, $query);

        if (mysqli_num_rows($query_run) > 0) {
            $row = mysqli_fetch_assoc($query_run);
            $emr_no1 = $row['emr_no'];
            $complaint = $row['medication'];
            $type = $row['prescr_date'];

            $priority = $row['diagnosis'];


    ?>

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Client Profile</h1>

            </div>
            <div class="row">

                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <!-- <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="..."> -->
                                    <h5 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h5>
                                </a>
                                <p class="description text-center">
                                    EMR #: <?php echo $row['emr_no']; ?>
                                </p>
                            </div>

                        </div>
                        <div class="card-footer">
                            <?php
                            $emr1 = $row['emr_no'];
                            $queryvitals1 = "SELECT * FROM vitals WHERE emr_no = $emr1 ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                            $query_run_vitals1 = mysqli_query($conn, $queryvitals1);
                            if (mysqli_num_rows($query_run_vitals1) > 0) {
                                $row_vitals1 = mysqli_fetch_assoc($query_run_vitals1);
                                $timefromdatabase = strtotime($row_vitals1['date_taken']);

                                $dif = time() - $timefromdatabase;
                                if ($dif < 86400) {
                            ?>

                                    <h5 class="title text-center">Vitals (<?php echo $row_vitals1['date_taken']; ?>)</h5>
                            <?php
                                } else {
                                    echo "<h5 class='title text-center'>Vitals </h5>";
                                }
                            } else {
                                echo "<h5 class='title text-center'>Vitals </h5>";
                            }

                            ?>
                            <hr>
                            <div class="button-container">
                                <?php
                                $emr = $row['emr_no'];
                                $weight = 1;
                                $temparature = 1;
                                $BP = 1;
                                $height = 1;
                                $queryvitals = "SELECT * FROM vitals WHERE emr_no = $emr ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                                $query_run_vitals = mysqli_query($conn, $queryvitals);
                                if (mysqli_num_rows($query_run_vitals) > 0) {
                                    $row_vitals = mysqli_fetch_assoc($query_run_vitals);
                                    $timefromdatabase = strtotime($row_vitals['date_taken']);

                                    $dif = time() - $timefromdatabase;
                                    if ($dif < 86400) {
                                ?>
                                        <div class="row">
                                            <div class="col-lg-3 mr-auto">
                                                <h6 class="text-info"><?php echo $row_vitals['wt'];
                                                                        $weight = $row_vitals['temp']; ?>Kgs
                                                    <br>
                                                    <small>Weight</small>
                                                </h6>
                                            </div>
                                            <div class="col-lg-3 mr-auto">
                                                <h6 class="text-warning"><?php echo $row_vitals['temp'];
                                                                            $temparature = $row_vitals['temp']; ?> &#8451;
                                                    <br>
                                                    <small>Temparature</small>
                                                </h6>
                                            </div>
                                            <div class="col-lg-3 mr-auto">
                                                <h6><?php echo $row_vitals['bp_sys']; ?> / <?php echo $row_vitals['bp_dia'];
                                                                                            $BP = $row_vitals['bp_sys'] . "/" . $row_vitals['bp_dia']; ?>
                                                    <br>
                                                    <small>BP</small>
                                                </h6>
                                            </div>
                                            <div class="col-lg-3 mr-auto">
                                                <h6 class="text-info"><?php echo $row_vitals['height'];
                                                                        $height = $row_vitals['height']; ?> cm
                                                    <br>
                                                    <small>Height</small>
                                                </h6>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 mr-auto">
                                                <h6 class="text-info"><?php echo $row_vitals['pulse']; ?>
                                                    <br>
                                                    <small>Pulse</small>
                                                </h6>
                                            </div>
                                            <div class="col-lg-3 mr-auto">
                                                <h6 class="text-warning"><?php echo $row_vitals['rr']; ?>
                                                    <br>
                                                    <small>RR</small>
                                                </h6>
                                            </div>
                                        </div>
                                <?php
                                    } else {
                                        echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                                    }
                                } else {
                                    echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                                }

                                ?>
                                <?php
                                $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                                $query3 = "SELECT * FROM art_initiation_visit  WHERE emr_no = $emr_no1";
                                $query_run3 = mysqli_query($conn, $query3);
                                if (mysqli_num_rows($query_run3) > 0) {
                                    echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PillCountModal'>Pill Adherence</button>";
                                }
                                ?>


                            </div>
                            <hr>
                            <h5 class="title text-center">Prescription: <?php echo $type; ?> </h5>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12 mr-auto">
                                    <p>
                                        <?php echo $complaint; ?>
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Forward to Department</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-3 mr-auto">
                                    <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#LabsModal'>Lab</button>
                                </div>
                                <div class="col-lg-3 mr-auto">
                                    <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#HTSsModal'>HTS</button>
                                </div>
                                <div class="col-lg-3 mr-auto">
                                    <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#DismissModal'>Dismiss</button>
                                </div>
                                <div class="col-lg-3 mr-auto">
                                    <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PharmacysModal'>Pharmacy</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card card-user">
                        <div class="card-header">
                            <?php

                            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                                echo '<h2>' . $_SESSION['success'] . '</h2>';
                                unset($_SESSION['success']);
                            }

                            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                                echo '<h2>' . $_SESSION['status'] . '</h2>';
                                unset($_SESSION['status']);
                            }


                            ?>
                            <h5 class="card-title">Medication History</h5>
                            <?php

                            $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                            $query2 = "SELECT * FROM `prescription` WHERE emr_no =  $emr_no1 ORDER BY `prescription`.`filled` DESC";
                            $query_run2 = mysqli_query($conn, $query2);



                            ?>
                            <div class="table-responsive">
                                <form action="functions/prescription.php" method="POST">
                                    <table>
                                        <tr>
                                            <th>Date</th>
                                            <th>Diagnosis</th>


                                            <!-- <th>Temperature</th>
                                <th>Pulse Rate</th>-->
                                            <th>Medication</th>
                                            <th>Dosage</th>
                                            <th>Filled Prescription</th>
                                            <th>Comment</th>
                                        </tr>
                                        <?php
                                        $filled_status = false;
                                        if (mysqli_num_rows($query_run2) > 0) {
                                            while ($row2 = mysqli_fetch_assoc($query_run2)) {
                                        ?>
                                                <tr>
                                                    <td><?php echo $row2['prescr_date'] ?></td>
                                                    <td><?php echo $row2['diagnosis'] ?></td>

                                                    <td><?php echo $row2['medication'] ?></td>
                                                    <td><?php echo $row2['dosage_form'] ?>
                                                        <?php if ($row2['filled'] == 0) { ?>
                                                            <input type="hidden" id='presc_id' name='presc_id[]' value="<?php {
                                                                                                                            echo $row2['presc_id'];
                                                                                                                        }  ?>">
                                                        <?php } ?>
                                                    </td>
                                                    <td><?php if ($row2['filled'] == 0) {
                                                            echo  "<select id='filled' name='filled[]' class='form-control' required>
                                            <option value=''>Select Option</option>
                                                <option value='1'>Filled</option>
                                               
                                                <option value='2'>Rejected</option>

                                                </select>";
                                                            $filled_status = true;
                                                        } else if ($row2['filled'] == 1) echo "filled";
                                                        else if ($row2['filled'] == 2) echo "Rejected"; ?></td>
                                                    <td><?php if ($row2['filled'] == 0) echo  "<input type='text' id='comment' name='comment[]' class='form-control'> ";
                                                        else if ($row2['filled'] == 1) echo $row2['comment'];
                                                        else if ($row2['filled'] == 2) echo $row2['comment']; ?></td>
                                                </tr>

                                        <?php

                                            }
                                        } else {
                                            echo "<tr>No records found</tr>";
                                        }

                                        ?>
                                    </table>


                            </div>

                        </div>


                    </div>
                    <?php
                    if ($filled_status) { ?>
                        <br>
                        <button type="submit" style="padding-top: 20px" name="prescription" id="prescription" class="btn btn-primary btn-round">Update Prescription</button>
                    <?php } ?>
                    </form>

                <?php
                if (isset($_POST['art_med'])) {

                    include('includes/art_med.php');
                }
            }

                ?>
                </div>
            </div>


</div>
<?php

    } else if (isset($_REQUEST['emr_no'])) {
        $emr_no = $_REQUEST['emr_no'];
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query = "SELECT * FROM clients , `prescription` WHERE `prescription`.`emr_no` = `clients`.`emr_no`  AND `clients`.`emr_no` = $emr_no  limit $start, 1";
        $query_run = mysqli_query($conn, $query);




        if (mysqli_num_rows($query_run) > 0) {
            $row = mysqli_fetch_assoc($query_run);
            $emr_no1 = $row['emr_no'];
            $complaint = $row['medication'];
            $type = $row['prescr_date'];

            $priority = $row['diagnosis'];


?>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Client Profile</h1>

    </div>
    <div class="row">

        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <div class="author">
                        <a href="#">
                            <!-- <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="..."> -->
                            <h5 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h5>
                        </a>
                        <p class="description text-center">
                            EMR #: <?php echo $row['emr_no']; ?>
                        </p>
                    </div>

                </div>
                <div class="card-footer">
                    <?php
                    $emr1 = $row['emr_no'];
                    $queryvitals1 = "SELECT * FROM vitals WHERE emr_no = $emr1 ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                    $query_run_vitals1 = mysqli_query($conn, $queryvitals1);
                    if (mysqli_num_rows($query_run_vitals1) > 0) {
                        $row_vitals1 = mysqli_fetch_assoc($query_run_vitals1);
                        $timefromdatabase = strtotime($row_vitals1['date_taken']);

                        $dif = time() - $timefromdatabase;
                        if ($dif < 86400) {
                    ?>

                            <h5 class="title text-center">Vitals (<?php echo $row_vitals1['date_taken']; ?>)</h5>
                    <?php
                        } else {
                            echo "<h5 class='title text-center'>Vitals </h5>";
                        }
                    } else {
                        echo "<h5 class='title text-center'>Vitals </h5>";
                    }

                    ?>
                    <hr>
                    <div class="button-container">
                        <?php
                        $emr = $row['emr_no'];
                        $weight = 1;
                        $temparature = 1;
                        $BP = 1;
                        $height = 1;
                        $queryvitals = "SELECT * FROM vitals WHERE emr_no = $emr ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                        $query_run_vitals = mysqli_query($conn, $queryvitals);
                        if (mysqli_num_rows($query_run_vitals) > 0) {
                            $row_vitals = mysqli_fetch_assoc($query_run_vitals);
                            $timefromdatabase = strtotime($row_vitals['date_taken']);

                            $dif = time() - $timefromdatabase;
                            if ($dif < 86400) {
                        ?>
                                <div class="row">
                                    <div class="col-lg-3 mr-auto">
                                        <h6 class="text-info"><?php echo $row_vitals['wt'];
                                                                $weight = $row_vitals['temp']; ?>Kgs
                                            <br>
                                            <small>Weight</small>
                                        </h6>
                                    </div>
                                    <div class="col-lg-3 mr-auto">
                                        <h6 class="text-warning"><?php echo $row_vitals['temp'];
                                                                    $temparature = $row_vitals['temp']; ?> &#8451;
                                            <br>
                                            <small>Temparature</small>
                                        </h6>
                                    </div>
                                    <div class="col-lg-3 mr-auto">
                                        <h6><?php echo $row_vitals['bp_sys']; ?> / <?php echo $row_vitals['bp_dia'];
                                                                                    $BP = $row_vitals['bp_sys'] . "/" . $row_vitals['bp_dia']; ?>
                                            <br>
                                            <small>BP</small>
                                        </h6>
                                    </div>
                                    <div class="col-lg-3 mr-auto">
                                        <h6 class="text-info"><?php echo $row_vitals['height'];
                                                                $height = $row_vitals['height']; ?> cm
                                            <br>
                                            <small>Height</small>
                                        </h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 mr-auto">
                                        <h6 class="text-info"><?php echo $row_vitals['pulse']; ?>
                                            <br>
                                            <small>Pulse</small>
                                        </h6>
                                    </div>
                                    <div class="col-lg-3 mr-auto">
                                        <h6 class="text-warning"><?php echo $row_vitals['rr']; ?>
                                            <br>
                                            <small>RR</small>
                                        </h6>
                                    </div>
                                </div>
                        <?php
                            } else {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                            }
                        } else {
                            echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                        }

                        ?>
                        <?php
                        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                        $query3 = "SELECT * FROM art_initiation_visit  WHERE emr_no = $emr_no1";
                        $query_run3 = mysqli_query($conn, $query3);
                        if (mysqli_num_rows($query_run3) > 0) {
                            echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PillCountModal'>Pill Adherence</button>";
                        }
                        ?>


                    </div>
                    <hr>
                    <h5 class="title text-center">Prescription: <?php echo $type; ?> </h5>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12 mr-auto">
                            <p>
                                <?php echo $complaint; ?>
                            </p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Forward to Department</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3 mr-auto">
                            <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#LabsModal'>Lab</button>
                        </div>
                        <div class="col-lg-3 mr-auto">
                            <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#HTSsModal'>HTS</button>
                        </div>
                        <div class="col-lg-3 mr-auto">
                            <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#DismissModal'>Dismiss</button>
                        </div>
                        <div class="col-lg-3 mr-auto">
                            <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PharmacysModal'>Pharmacy</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-user">
                <div class="card-header">
                    <?php

                    if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                        echo '<h2>' . $_SESSION['success'] . '</h2>';
                        unset($_SESSION['success']);
                    }

                    if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                        echo '<h2>' . $_SESSION['status'] . '</h2>';
                        unset($_SESSION['status']);
                    }


                    ?>
                    <h5 class="card-title">Medication History</h5>
                    <?php

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query2 = "SELECT * FROM `prescription` WHERE emr_no =  $emr_no1 AND `prescription`.`filled` = '2' ORDER BY `prescription`.`filled` DESC";
                    $query_run2 = mysqli_query($conn, $query2);



                    ?>
                    <div class="table-responsive">
                        <form action="functions/prescription.php" method="POST">
                            <table>
                                <tr>
                                    <th>Date</th>
                                    <th>Diagnosis</th>


                                    <!-- <th>Temperature</th>
                                <th>Pulse Rate</th>-->
                                    <th>Medication</th>
                                    <th>Dosage</th>
                                    <th>Dose</th>
                                    <th>Filled Prescription</th>
                                    <th>Comment</th>
                                </tr>
                                <?php

                                if (mysqli_num_rows($query_run2) > 0) {
                                    while ($row2 = mysqli_fetch_assoc($query_run2)) {
                                ?>
                                        <tr>
                                            <td><?php echo $row2['prescr_date'] ?></td>
                                            <td><?php echo $row2['diagnosis'] ?><select name='diag[]' id='diag' style="width: 100%" class="form-control" required>
                                                    <option value=''>Select</option>
                                                    <option value="ART Medication">ART Medication</option>
                                                    <option value="Family Planning">Family Planning</option>
                                                    <option value="OPD Medication">OPD Medication</option>

                                                </select></td>

                                            <td><?php echo $row2['medication'] ?>
                                                <?php if ($row2['filled'] == 2) { ?><select name='meds[]' id='meds' style="width: 100%" class="form-control" required>
                                                        <option value=''>Select</option>
                                                        <?php
                                                        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                                                        $query = "SELECT * FROM `medicine_master` ";
                                                        $query_run = mysqli_query($conn, $query);
                                                        if (mysqli_num_rows($query_run) > 0) {
                                                            while ($row = mysqli_fetch_assoc($query_run)) {
                                                        ?>
                                                                <option value="<?php echo $row['name']; ?>"><?php echo $row['name'] . " " . $row['dose'] . " " . $row['mg_ml']; ?></option>
                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                                        <!-- <option value="Good Shepherd">Good Shepherd</option>
                                    <option value="Lancet">Lancet</option>
                                    <option value="PIMA">PIMA</option>
                                    <option value="Amphath">Amphath</option>
                                    <option value="Other">Lancet</option> -->
                                                    </select> <?php } else if ($row2['filled'] == 1) echo $row2['medication'];
                                                            else if ($row2['filled'] == 0) echo $row2['medication']; ?></td>
                                            <td><?php echo $row2['dosage_form'] ?>
                                                <?php if ($row2['filled'] == 2) echo  "<input type='text' id='dosage_form' name='dosage_form[]' class='form-control'> ";
                                                else if ($row2['filled'] == 1) echo $row2['dosage_form'];
                                                else if ($row2['filled'] == 0) echo $row2['dosage_form']; ?>
                                                <?php if ($row2['filled'] == 2) { ?>
                                                    <input type="hidden" id='presc_id' name='presc_id[]' value="<?php {
                                                                                                                    echo $row2['presc_id'];
                                                                                                                }  ?>">
                                                <?php } ?>
                                            </td>
                                            <td><?php echo $row2['dose'] ?>
                                            <?php if ($row2['filled'] == 2) echo  "<input type='text' id='dose' name='dose[]' class='form-control'> ";
                                                else if ($row2['filled'] == 1) echo $row2['dose'];
                                                else if ($row2['filled'] == 0) echo $row2['dose']; ?></td>
                                            <td><?php if ($row2['filled'] == 0) echo  "<select id='filled' name='filled[]' class='form-control' required>
                                            <option value=''>Select Option</option>
                                                <option value='1'>Filled</option>
                                               
                                                <option value='2'>Rejected</option>

                                                </select>";
                                                else if ($row2['filled'] == 1) echo "filled";
                                                else if ($row2['filled'] == 2) echo "Rejected"; ?></td>
                                            <td><?php if ($row2['filled'] == 0) echo  "<input type='text' id='comment' name='comment[]' class='form-control'> ";
                                                else if ($row2['filled'] == 1) echo $row2['comment'];
                                                else if ($row2['filled'] == 2) echo $row2['comment']; ?></td>
                                        </tr>

                                <?php

                                    }
                                } else {
                                    echo "<tr>No records found</tr>";
                                }

                                ?>
                            </table>
                            <button type="submit" name="prescr_review" id="prescr_review" class="btn btn-primary btn-round">Update Prescription</button>
                        </form>
                    </div>
                </div>

            </div>

        <?php
            if (isset($_POST['art_med'])) {

                include('includes/art_med.php');
            }
        }

        ?>
        </div>
    </div>


    </div>
<?php

    } else {
        echo "No records found";
    }

?>
</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bps" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="rr" name="rr">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="NurseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="pharm.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="DismissModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Dismiss Home</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Dismiss</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="pharm.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="dismiss" id="dismiss" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>