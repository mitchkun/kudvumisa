<?php

require "includes/config.php";

include('includes/header.php');
include('includes/navbar.php');
?>


<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
  </div>

  <!-- Content Row -->
  <div class="row">
    <?php
    if (!isset($_SESSION) || $_SESSION['permission'] == 0 || $_SESSION['permission'] == 1 || $_SESSION['permission'] == 2 || $_SESSION['permission'] == 3 || $_SESSION['permission'] == 4 || $_SESSION['permission'] == 5) {
    ?>
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <a href="newreception.php">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-l font-weight-bold text-primary text-uppercase mb-1">
                    <h2 style="font-weight: 15em;">RECEPTION</h2>
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">

                    <h6>Total Clients in Queue: *</h6>

                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-calendar fa-3x text-primary"></i>

                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    <?php
    }
    if (!isset($_SESSION) || $_SESSION['permission'] == 0 || $_SESSION['permission'] == 2) {
    ?>
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
          <a href="nurse.php">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    <h2 style="font-weight: 15em;">NURSE</h2>
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query = "SELECT COUNT(id) AS clients FROM `queue` WHERE department = 'Nurse'";
                    $query_run = mysqli_query($conn, $query);
                    if (mysqli_num_rows($query_run) > 0) {
                      $row = mysqli_fetch_assoc($query_run);


                    ?>
                      <h6>Total Clients in Queue: <?php echo $row['clients']; ?></h6>
                    <?php
                    } else {
                      echo "No records found";
                    }
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-user-nurse fa-3x text-success"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    <?php
    }
    if (!isset($_SESSION) || $_SESSION['permission'] == 0 || $_SESSION['permission'] == 3 || $_SESSION['permission'] == 2 || $_SESSION['permission'] == 4) {
    ?>
      <!-- Earnings (Monthly) Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
          <a href="lab.php">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                    <h2 style="font-weight: 15em;">LAB</h2>
                  </div>
                  <div class="row no-gutters align-items-center">
                    <div class="col-auto">
                      <div class="h5 mb-0 font-weight-bold text-gray-800">
                        <?php

                        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                        $query = "SELECT COUNT(id) AS clients FROM `queue` WHERE department = 'Lab'";
                        $query_run = mysqli_query($conn, $query);
                        if (mysqli_num_rows($query_run) > 0) {
                          $row = mysqli_fetch_assoc($query_run);


                        ?>
                          <h6>Total Clients in Queue: <?php echo $row['clients']; ?></h6>
                        <?php
                        } else {
                          echo "No records found";
                        }
                        ?>
                      </div>
                    </div>
                    <!-- <div class="col">
                  <div class="progress progress-sm mr-2">
                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50"
                      aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div> -->
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-vials fa-3x text-info"></i>
                </div>
              </div>
            </div>
          </a>
        </div>

      </div>
    <?php
    }
    if (!isset($_SESSION) || $_SESSION['permission'] == 0 || $_SESSION['permission'] == 4 || $_SESSION['permission'] == 2) {
    ?>
      <!-- Pending Requests Card Example -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <a href="hts.php">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                    <h2 style="font-weight: 15em;">HTS</h2>
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query = "SELECT COUNT(id) AS clients FROM `queue` WHERE department = 'Hts'";
                    $query_run = mysqli_query($conn, $query);
                    if (mysqli_num_rows($query_run) > 0) {
                      $row = mysqli_fetch_assoc($query_run);


                    ?>
                      <h6>Total Clients in Queue: <?php echo $row['clients']; ?></h6>
                    <?php
                    } else {
                      echo "No records found";
                    }
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-comments fa-3x text-warning"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    <?php
    }
    if (!isset($_SESSION) || $_SESSION['permission'] == 0 || $_SESSION['permission'] == 5 || $_SESSION['permission'] == 2) {
    ?>
      <!-- Pending Requests Card Example -->
      <div class="col-xl-3 col-md-6 mb-5">
        <div class="card border-left-danger shadow h-100 py-2">
          <a href="pharm.php">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                    <h2 style="font-weight: 15em;">PHARM</h2>
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query = "SELECT COUNT(DISTINCT emr_no) AS clients FROM `prescription` WHERE filled = '0'";
                    $query_run = mysqli_query($conn, $query);
                    if (mysqli_num_rows($query_run) > 0) {
                      $row = mysqli_fetch_assoc($query_run);


                    ?>
                      <h6>Total Clients in Queue: <?php echo $row['clients']; ?></h6>
                    <?php
                    } else {
                      echo "No records found";
                    }
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-tablets fa-3x text-danger"></i>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div><?php
          }

            ?>
  </div>

  <!-- Content Row -->








  <?php
  include('includes/scripts.php');
  include('includes/footer.php');
  ?>