<?php
$conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
$query = "SELECT * FROM art_initiation_visit WHERE `emr_no` = $emr1  ";
$query_run = mysqli_query($conn, $query);


if (mysqli_num_rows($query_run) > 0) {
    $row = mysqli_fetch_assoc($query_run)
?>

    <div class="card card-user" style="margin-top: 20px">
        <div class="card-header">
            <?php

            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo '<h2>' . $_SESSION['success'] . '</h2>';
                unset($_SESSION['success']);
            }

            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo '<h2>' . $_SESSION['status'] . '</h2>';
                unset($_SESSION['status']);
            }
            //$emr_no = $_REQUEST['emr_no'];




            ?>
            <h5 class="card-title">ART INITIATION VISIT</h5>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label>Weight/Height</label>
                        <input type="text" id="weight" disabled="" name="weight" class="form-control" value="<?php echo $row['weight']; ?>">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>BMI</label>
                        <input type="text" id="bmi" disabled="" name="bmi" class="form-control" value="<?php echo $row['height']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label for="exampleInputEmail1">MUAC</label>
                        <input type="text" id="muac" name="muac" disabled="" class="form-control" value="<?php echo $row['muac']; ?>">
                        <input type="hidden" id="vitals_id" name="vitals_id" class="form-control" value="<?php echo $vitalsid; ?>">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Nutritional Status</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['nutritional_status']; ?>">

                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Functional Status</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['functional_status']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>TB Screening</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['tb_screening']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>WHO Stage</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['who_stage']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>CD4 Count</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['cd4']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Hb</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['hb']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>ALT Results</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['alt']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>AST Results</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['ast']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>CREAT Results</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['creatinine']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label style="margin: 10px">Recent Ol Hx</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['recent_ol_hx']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Obstetric Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Parity</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['obsteric_hx_parity']; ?>">
                    </div>
                </div>

                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>PMTCT</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['pmtct']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Regimen</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['regimen']; ?>">
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Pregnant</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['pregnant']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>LMP</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['lmp']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Trimester</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['pregnant']; ?>">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>EDD</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['edd']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>FP</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['fp']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>HIV Exposed Infant Prophylaxis</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['hiv_infant_prophylaxis']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Drug Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Past ART</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['drug_hx_past_art']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Date</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['drug_hx_past_art_date']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Regimen</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['drug_hx_past_art_regimen']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>On CTX</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['drug_hx_past_art_ctx']; ?>">
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Date</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['on_ctx_date']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Current Medications 1</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['current_med_1']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Current Medications 2</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['current_med_2']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Current Medications 3</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['current_med_3']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Social Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Smoking</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['smoking']; ?>">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Alcohol</label><br />
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['alcohol']; ?>">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Consistent Condom Use</label>
                        <input type="text" id="vitals_id" disabled="" name="vitals_id" class="form-control" value="<?php echo $row['consistent_condom_use']; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 pl-1" style="margin: 10px">Family HTC Hx:</label>
                <label class="col-md-2 pl-1">Age</label>
                <label class="col-md-3 pl-1">HIV Status</label>
                <label class="col-md-3 pl-1">On ART</label>
            </div>
            <?php
            $query1 = "SELECT * FROM art_init_family_htc WHERE `art_init_id` = " . $row['art_init_id'];
            $query_run1 = mysqli_query($conn, $query1);
            if (mysqli_num_rows($query_run1) > 0) {
                while ($row1 = mysqli_fetch_assoc($query_run1)) {
            ?>
                    <div class="row">
                        <div class="col-md-2 pr-1">
                            <div class="form-group">
                                <label><?php echo $row1['relation']; ?></label>
                            </div>
                        </div>
                        <div class="col-md-2 px-1">
                            <div class="form-group">
                                <input type="text" id="spouse_age" disabled="" name="spouse_age" class="form-control" value="<?php echo $row1['age']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <input type="text" id="spouse_hiv" disabled="" name="spouse_hiv" class="form-control" value="<?php echo $row1['hiv_status']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <input type="text" id="spouse_on_art" disabled="" name="spouse_on_art" class="form-control" value="<?php echo $row1['on_art']; ?>">
                            </div>
                        </div>
                    </div>
                <?php

                }
            } else echo "<div class='row'><div class='col-md-12 pr-1'>No Records</div></div>";
            ?>
                    <div class="row">
                        <label style="margin: 10px">Examination: General</label>
                    </div>
            <?php
        $query2 = "SELECT * FROM art_init_examination WHERE `art_init_id` = " . $row['art_init_id'];
        $query_run2 = mysqli_query($conn, $query2);
        if (mysqli_num_rows($query_run2) > 0) {
            while ($row2 = mysqli_fetch_assoc($query_run2)) {

                ?>

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Head/neck</label>
                                <input type="text" id="head_neck" disabled="" name="head_neck" class="form-control" value="<?php echo $row2['head_neck']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Respiratory</label><br />
                                <input type="text" id="respiratory" disabled="" name="respiratory" class="form-control" value="<?php echo $row2['resp']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>GIT</label>
                                <input type="text" id="git" name="git" disabled="" class="form-control" value="<?php echo $row2['git']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>CNS</label>
                                <input type="text" id="cns" name="cns" disabled="" class="form-control" value="<?php echo $row2['cns']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label style="margin: 10px">Examination: Muscular Skeletal</label>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>MSS</label>
                                <input type="text" id="mss" name="mss" disabled="" class="form-control" value="<?php echo $row2['mss']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>CVS</label><br />
                                <input type="text" id="cvs" name="cvs" disabled="" class="form-control" value="<?php echo $row2['cvs']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>UGS</label>
                                <input type="text" id="ugs" name="ugs" disabled="" class="form-control" value="<?php echo $row2['ugs']; ?>">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Skin</label>
                                <input type="text" id="skin" name="skin" disabled="" class="form-control" value="<?php echo $row2['skin']; ?>">
                            </div>
                        </div>
                    </div>
            <?php

                }
            } else echo "<div class='row'><div class='col-md-12 pr-1'>No Records</div></div>";
            ?>
            <div class="row"><label style="margin: 10px">Associated Dx</label><input name="associate_dx"  disabled="" rows="2" style="margin: 10px" class="form-control col-md-15 pl-1" value="<?php echo $row['associated_dx']; ?>"></input></div>
            <div class="row">
                <label style="margin: 10px">ART - REGIMEN:</label>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>ADULT ART</label>
                        <input type="text" id="skin" name="skin" disabled="" class="form-control" value="<?php echo $row['adult_art']; ?>">
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>PEDIATRIC ART</label>
                        <input type="text" id="skin" name="skin" disabled="" class="form-control" value="<?php echo $row['pediatric_art']; ?>">
                    </div>
                </div>



            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Food Supplement</label>
                        <input type="text" id="skin" name="skin" disabled="" class="form-control" value="<?php echo $row['food_supplement']; ?>">
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Return Date</label><br />
                        <input type="date" id="return_date" name="return_date"  disabled=""class="form-control" value="<?php echo $row['return_date']; ?>">
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Care Entry Point</label>
                        <select name='cep' id='cep' style="width: 100%" class="form-control">
                            <option value='VCT'>VCT</option>
                            <option value='HTC'>HTC</option>
                            <option value='PMTCT'>PMTCT</option>
                            <option value='TB'>TB</option>
                            <option value='STI'>STI</option>
                            <option value='U5 Clinic'>U5 CliniC</option>
                            <option value='DBS'>DBS</option>
                            <option value='OPD'>OPD</option>
                            <option value='WARD'>WARD</option>
                            <option value='Pre-ART'>Pre-ART</option>
                            <option value='OUTREACH'>OUTREACH</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Other (Specify)</label><br />
                        <input type="text" id="cep_other" name="cep_other" class="form-control">
                    </div>
                </div>
            </div> -->
            
        </div>
    <?php

} else {
    echo "<a href='newreception.php' >No records found</a>";
}

    ?>