  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script>
  <script>
      var btn_start = document.getElementById('bt-start');
      btn_start.addEventListener('mgo-fingerprint-capture-event', (evt) => {
          console.log('Fingerprint base64', evt.detail)
          // Do something else
      });
  </script>
  <script>
      function calculatetth() {
          var val1 = parseInt($('#dlv').val());
          var val2 = parseInt($('#remaining').val());
          var sum = 0;

          sum = val1 + val2;
          document.getElementById("tth").value = sum;

      }

      function calculatetslv() {
          var val1 = parseInt($('#tth').val());
          var val2 = parseInt($('#rt').val());
          var sum = 0;

          sum = val1 - val2;
          document.getElementById("tslv").value = sum;

      }

      function calculateat() {
          var val1 = parseInt($('#tslv').val());
          var val2 = parseInt($('#rnt').val());
          var sum = 0;

          sum = val1 - val2;
          document.getElementById("at").value = sum;

      }

      function calculatedays(value_input) {
          var dateval = document.getElementById("visit_date").textContent;
          var dateval1 = document.getElementById("refill_date").textContent;
          var date2 = new Date(dateval);
          var date1 = new Date(dateval1);

          var difference = date1.getTime() - date2.getTime();
          var days = difference / (1000 * 3600 * 24);
          document.getElementById("dslr").value = Math.floor(days); //document.getElementById('visit_date').innerHTML;
          var et = Math.floor(days) * value_input;
          document.getElementById("et").value = et;

          var g = parseInt($('#at').val());
          var perc = Math.round((g / et) * 100);
          document.getElementById("padher").value = perc;

          if (perc <= 94 && perc >= 90) {
              document.getElementById("adher").value = "Poor";
          } else if (perc < 90) {
              document.getElementById("adher").value = "Very Poor";
          } else if (perc <= 105 && perc >= 95) {
              document.getElementById("adher").value = "Good";
          } else if (perc > 105) {
              document.getElementById("adher").value = "Excessive";
          } else {
              document.getElementById("adher").value = "N/A";
          }
      }

      $(document).ready(function() {
          $("#mybutton").trigger("click");
      });
  </script>


  <?php


    $connection = mysqli_connect("localhost:3308", "root", "", "kudvumisa");

    if (isset($_POST['registerbtn'])) {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirm_password = $_POST['confirmpassword'];

        if ($password === $confirm_password) {
            $query = "INSERT INTO register (username,email,password) VALUES ('$username','$email','$password')";
            $query_run = mysqli_query($connection, $query);

            if ($query_run) {
                echo "done";
                $_SESSION['success'] =  "Admin is Added Successfully";
                header('Location: register.php');
            } else {
                echo "not done";
                $_SESSION['status'] =  "Admin is Not Added";
                header('Location: register.php');
            }
        } else {
            echo "pass no match";
            $_SESSION['status'] =  "Password and Confirm Password Does not Match";
            header('Location: register.php');
        }
    }

    ?>