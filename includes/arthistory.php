<?php
$conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
$query = "SELECT * FROM art_initiation_visit WHERE `emr_no` = $emr1  ";
$query_run = mysqli_query($conn, $query);



?>

<div class="card card-user" style="margin-top: 20px">
  <div class="card-header">
    <?php

    if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
      echo '<h2>' . $_SESSION['success'] . '</h2>';
      unset($_SESSION['success']);
    }

    if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
      echo '<h2>' . $_SESSION['status'] . '</h2>';
      unset($_SESSION['status']);
    }
    //$emr_no = $_REQUEST['emr_no'];




    ?>
    <h5 class="card-title">ART INITIATION VISIT</h5>
  </div>
  <div class="card-body">

    <div class="container-fluid">

      <!-- Page Heading -->
      <h1 class="h3 mb-4 text-gray-800">Patient Data</h1>
      <div class="table-wrapper">
        <!-- start table -->
        <table class="table table-hover table-bordered ">
          <thead>
            <!-- start headings -->
            <!-- heading row 1 -->

            <tr class="heading">
              <th rowspan="4">Visit Date</th>
              <th>Weight(kg)</th>
              <th>Nutritional Status</th>
              <th>WHO Stage</th>
              <th>Haemoglobin</th>
              <th>TB Screen</th>
              <th>Pregn. EDD</th>
              <th rowspan="4">Adherence(%)</th>
              <th>Comments</th>
              <th></th>

            </tr>

            <!-- heading row 2 -->

            <tr class="heading">
              <th>Height(cm)</th>
              <th>For Food Supplement</th>
              <th>Function Status</th>
              <th>HB</th>
              <th>Cervical Cancer Screening</th>
              <th>CTX</th>
              <th>Next Visit</th>
              <th>Sign</th>
            </tr>

            <!-- heading row 3 -->

            <tr class="heading">
              <th>BMI</th>
              <th>BP(mmHg)</th>
              <th>Cd4 Count(%)</th>
              <th>ALT</th>
              <th>Lactaction</th>
              <th>IPT</th>
            </tr>

            <!-- heading row 4 -->

            <tr class="heading">
              <th>MUAC(cm)</th>
              <th>T &#8451</th>
              <th>FP Use</th>
              <th>Creatinine</th>
              <th>STI Screening</th>
              <th>ANTI TB Rx</th>
            </tr>
          </thead>

          <!-- End headings -->

          <!-- Begin data rows -->
          <tbody>
            <!-- row 1 -->
            <tr class="data table-striped">
              <td rowspan="4">22/11/2020</td>
              <td>60</td>
              <td>3</td>
              <td>4</td>
              <td>13.6</td>
              <td>n</td>
              <td>Null</td> <!-- Pregn. EDD -->
              <td rowspan="4">40</td>
              <td>15/01/2021</td>
              <td>Null</td> <!-- Sign -->
            </tr>

            <tr class="data table-striped">
              <td>150</td>
              <td>Y</td>
              <td>W</td>
              <td>13.6</td>
              <td>n</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>1.3</td>
              <td>140/90</td>
              <td>600</td>
              <td>7</td>
              <td>Y</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>100</td>
              <td>2</td>
              <td>5</td>
              <td>74.6</td>
              <td>Y</td>
              <td>N</td>
            </tr>

            <!-- row 2 -->
            <tr class="data">
              <td rowspan="4">22/11/2020</td>
              <td>60</td>
              <td>3</td>
              <td>4</td>
              <td>13.6</td>
              <td>n</td>
              <td>Null</td> <!-- Pregn. EDD -->
              <td rowspan="4">40</td>
              <td>15/01/2021</td>
              <td>Null</td> <!-- Sign -->
            </tr>

            <tr class="data">
              <td>150</td>
              <td>Y</td>
              <td>W</td>
              <td>13.6</td>
              <td>n</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>1.3</td>
              <td>140/90</td>
              <td>600</td>
              <td>7</td>
              <td>Y</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>100</td>
              <td>2</td>
              <td>5</td>
              <td>74.6</td>
              <td>Y</td>
              <td>N</td>
            </tr>

            <!-- row 3 -->
            <tr class="data">
              <td rowspan="4">22/11/2020</td>
              <td>60</td>
              <td>3</td>
              <td>4</td>
              <td>13.6</td>
              <td>n</td>
              <td>Null</td> <!-- Pregn. EDD -->
              <td rowspan="4">40</td>
              <td>15/01/2021</td>
              <td>Null</td> <!-- Sign -->
            </tr>

            <tr class="data">
              <td>150</td>
              <td>Y</td>
              <td>W</td>
              <td>13.6</td>
              <td>n</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>1.3</td>
              <td>140/90</td>
              <td>600</td>
              <td>7</td>
              <td>Y</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>100</td>
              <td>2</td>
              <td>5</td>
              <td>74.6</td>
              <td>Y</td>
              <td>N</td>
            </tr>

            <!-- row 4 -->
            <tr class="data">
              <td rowspan="4">22/11/2020</td>
              <td>60</td>
              <td>3</td>
              <td>4</td>
              <td>13.6</td>
              <td>n</td>
              <td>Null</td> <!-- Pregn. EDD -->
              <td rowspan="4">40</td>
              <td>15/01/2021</td>
              <td>Null</td> <!-- Sign -->
            </tr>

            <tr class="data">
              <td>150</td>
              <td>Y</td>
              <td>W</td>
              <td>13.6</td>
              <td>n</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>1.3</td>
              <td>140/90</td>
              <td>600</td>
              <td>7</td>
              <td>Y</td>
              <td>Y</td>
            </tr>

            <tr class="data">
              <td>100</td>
              <td>2</td>
              <td>5</td>
              <td>74.6</td>
              <td>Y</td>
              <td>N</td>
            </tr>

            <!-- End data rows -->
          </tbody>
        </table>
      </div>

      <!-- End table -->

    </div>


  </div>
</div>