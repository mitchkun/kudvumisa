<div class="col-md-8">
    <div class="card card-user" style="margin-top: 20px">
        <div class="card-header">
            
            <h5 class="card-title">CD4 Count Test</h5>
        </div>
        <div class="card-body">
            <form action="functions/drawlab.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <label>Date Drawn</label>
                            <input type="date" id="date_drawn" name="date_drawn" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Purpose of Test</label>
                            <input type="text" id="purpose" name="purpose" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4 px-1">
                        <div class="form-group">
                            <label>Testing Facility</label>
                            <select name='test_fac' id='test_fac' style="width: 100%" class="form-control" required>
                                    <option value=''>Select</option>
                                    <option value="In-House">In House</option>
                                    <option value="Good Shepherd">Good Shepherd</option>
                                    <option value="Lancet">Lancet</option>
                                    <option value="PIMA">PIMA</option>
                                    <option value="Amphath">Amphath</option>
                                    <option value="Other">Lancet</option>
                                </select>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <label>Other Facility</label>
                            <input type="text" id="other_fac" name="other_fac" class="form-control">
                        </div>
                    </div>
                    

                </div>

               

                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" name="cd4" id="cd4" class="btn btn-primary btn-round">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>