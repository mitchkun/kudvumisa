<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">Index Register : <?php echo date("Y-m-d"); ?></h2>
    </div>
    <div class="card-body">
        <form action="functions/artinit_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Name and Surname</label>
                        <br>
                        <input type="text" id="name_surname" name="name_surname" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Gender</label>
                        <select id='gender' name='gender' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Male'>Male</option>
                            <option value='Female'>Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Gender</label>
                        <input type="number" id="age" name="age" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Physical Address</label>
                        <br>
                        <input type="text" id="address" name="address" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="number" id="phone" name="phone" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Relationship with Index</label>
                        <select id='relation' name='relation' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Partner'>Partner</option>
                            <option value='Child'>Child</option>
                            <option value='Parent'>Parent</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Notification Strategy</label>
                        <br>
                        <select id='notification' name='notification' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Client Referral'>Client Referral</option>
                            <option value='Provider Referral'>Provider Referral</option>
                            <option value='Contract Referral'>Contract Referral</option>
                            <option value='Dual Referral'>Dual Referral</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Number of HIV self test kits</label>
                        <input type="number" id="self_kits" name="self_kits" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>HIVST Kit recieved</label>
                        <br>
                        <select id='hivst' name='hivst' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>HIV Self testing kit used</label>
                        <select id='hivst_used' name='hivst_used' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>HIVST Results</label>
                        <select id='hivst_used' name='hivst_used' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>R</option>
                            <option value='Nr'>Nr</option>
                            <option value='Incl'>Incl</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of Follow Up 1</label>
                        <br>
                        <input type="date" id="date_fup1" name="date_fup1" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Outcome 1</label>
                        <select id='outcome1' name='outcome1' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Traced and Eligible'>Traced and Eligible</option>
                            <option value='Traced not Eligible'>Traced not Eligible</option>
                            <option value='could not be traced'>could not be traced</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of Follow Up 2</label>
                        <br>
                        <input type="date" id="date_fup2" name="date_fup2" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Outcome 2</label>
                        <select id='outcome2' name='outcome2' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Traced and Eligible'>Traced and Eligible</option>
                            <option value='Traced not Eligible'>Traced not Eligible</option>
                            <option value='could not be traced'>could not be traced</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of Follow Up 3</label>
                        <br>
                        <input type="date" id="date_fup3" name="date_fup3" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Outcome 3</label>
                        <select id='outcome3' name='outcome3' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Traced and Eligible'>Traced and Eligible</option>
                            <option value='Traced not Eligible'>Traced not Eligible</option>
                            <option value='could not be traced'>could not be traced</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Date of final HIV test</label>
                        <br>
                        <input type="date" id="final_test" name="final_test" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Final HIV test results</label>
                        <input type="date" id="final_test_results" name="final_test_results" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Linkage to ART</label>
                        <input type="text" id="link_art" name="link_art" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>ART No</label>
                        <br>
                        <input type="text" id="art_num" name="art_num" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date Started</label>
                        <input type="date" id="date_started" name="date_started" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Reasons for delaying ART</label>
                        <br>
                        <input type="text" id="reason_art" name="reason_art" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Comments</label>
                        <input type="text" id="comments" name="comments" class="form-control">
                    </div>
                </div>
            </div> 
                      

    </div>

    <div class="row">
        <div class="update ml-auto mr-auto">
            <button type="submit" name="link_register" id="link_register" class="btn btn-primary btn-round">Submit</button>
        </div>
    </div>
    </form>
</div>