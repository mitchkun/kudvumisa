<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">HTS Register : <?php echo date("Y-m-d"); ?></h2>
    </div>
    <div class="card-body">
        <form action="functions/artinit_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Key Population</label>
                        <br>
                        <select id='key_pop' name='key_pop' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='1'>MSM</option>
                            <option value='2'>FSW</option>
                            <option value='3'>IDU</option>
                            <option value='4'>Prisoner</option>
                            <option value='5'>Uniform Forces</option>
                            <option value='6'>Trans-gender</option>
                            <option value='7'>N/A</option>
                            <option value='8'>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Key Population</label>
                        <input type="text" id="key_pop_other" name="key_pop_other" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Risk Assessment: Adults and adolescents</h4>
            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Had condomless sex with HIV positive partner who does not have a suppressed viral load or who is not (yet) on ART</label>
                        <select id='q1' name='q1' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Had a sec=xually transmitted infection (STI) in the last 6 months</label>
                        <select id='q2' name='q2' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Had sex while under the influence of alcohol</label>
                        <select id='q3' name='q3' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Had more than one sexual partner</label>
                        <select id='q4' name='q4' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Had more than one sexual partner in the last 12 months</label>
                        <select id='q5' name='q5' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Ever taken care of or came in contact with body fluids like blood, body secretions after a negative HIV test or in the past 2 months</label>
                        <select id='q6' name='q6' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Injecting drugs and sharing needles, syringes and other equipment with others.</label>
                        <select id='q7' name='q7' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>

            </div>
            
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Risk Assessment</h4>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Outcome</label>
                        <select id='risk_outcome' name='risk_outcome' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='1'>at risk</option>
                            <option value='2'>not at risk</option>
                            <option value='3'>N/A (Specify)</option>
                            
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>N/A Outcome</label>
                        <input type="text" id="risk_other" name="risk_other" class="form-control">
                    </div>
                </div>

            </div>
            
    </div>

    <div class="row">
        <div class="update ml-auto mr-auto">
            <button type="submit" name="hts_register" id="hts_register" class="btn btn-primary btn-round">Submit</button>
        </div>
    </div>
    </form>
</div>