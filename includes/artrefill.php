<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h5 class="card-title">ART REFILL: <?php echo $emr_no1; ?></h5>
    </div>
    <div class="card-body">
        <form action="functions/artinit_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Weight</label>
                        <input type="text" id="weight_txt" name="weight_txt" required class="form-control" disabled="" value="<?php echo $weight; ?>">
                        <input type="hidden" id="weight" name="weight" class="form-control" value="<?php echo $weight; ?>">
                    </div>
                </div>
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Height</label>
                        <input type="text" id="height_text" name="height_text" required class="form-control" disabled="" value="<?php echo $height; ?>">
                        <input type="hidden" id="height" name="height" class="form-control" value="<?php echo $height; ?>">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>BMI</label>
                        <input type="text" id="bmi_text" name="bmi_text" required class="form-control" disabled="" value="<?php echo round(($weight / (($height / 100) * ($height / 100))), 2); ?>">
                        <input type="hidden" id="bmi" name="bmi" class="form-control" value="<?php echo round(($weight / (($height / 100) * ($height / 100))), 2); ?>">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label for="exampleInputEmail1">MUAC</label>
                        <input type="text" id="muac" name="muac" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-6 pl-1">
                    <div class="form-group">
                        <label>Nutritional Status</label>
                        <?php

                        echo "<select name='ns' id='ns' class='form-control' required>
                                        <option value=''>Select Nutritional Status</option>
                                        <option value='0'>Normal</option>
                                        <option value='1'>Mild</option>
                                        <option value='2'>Moderate</option>
                                        <option value='3'>Severe</option>
                                        </select>";
                        ?>
                        <!-- <input type="radio" id="male" name="gender" value="male">
                                        <label for="male">Male</label>
                                        <input type="radio" id="female" name="gender" value="female" checked="true">
                                        <label for="female">Female</label> -->
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Food Supplement</label>
                        <select name='food_supp' id='food_supp' style="width: 100%" required class="form-control">
                            <option value=''>Select</option>
                            <option value='yes'>Yes</option>
                            <option value='no'>No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pl-1">
                    <div class="form-group">
                        <label>WHO T Stage</label>
                        <?php

                        echo "<select name='who_stage' id='who_stage' required class='form-control' required>
                        <option value=''>Select WHO Stage</option>
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-6 pl-1">
                    <div class="form-group">
                        <label>Functional Status</label>
                        <?php

                        echo "<select name='fs' id='fs' class='form-control' required>
                        <option value=''>Select Functional Status</option>
                        <option value=''>Select</option>
                                        <option value='Working'>Working</option>
                                        <option value='Ambulating'>Ambulating</option>
                                        <option value='Bedridden'>Bedridden</option>
                                        </select>";
                        ?>

                    </div>
                </div>
            </div>

            <div class="row">
                <!-- <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>WHO Stage</label>
                        <input type="text" id="who" name="who" class="form-control">
                    </div>
                </div> -->
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>CD4 Count %</label>
                        <input type="text" id="cd4" name="cd4" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>HB Results</label>
                        <input type="number" id="hb" name="hb" class="form-control">

                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Viral Load</label>
                        <input type="number" id="viral_load" name="viral_load" class="form-control">

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>ALT Results</label>
                        <input type="text" id="alt" name="alt" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>AST Results</label>
                        <input type="text" id="ast" name="ast" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>CREAT Results</label>
                        <input type="number" id="creat" name="creat" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Pregnancy EDD</label>
                        <input type="date" id="edd" name="edd" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Lactating</label>
                        <select name='lactating' id='lactating' style="width: 100%" class="form-control">
                            <option value=''>Select</option>
                            <option value='yes'>Yes</option>
                            <option value='no'>No</option>
                            <option value='n/a'>N/A</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Family Planning</label>
                        <select name='fp' id='fp' style="width: 100%" class="form-control">
                            <option value='0'>None</option>
                            <option value='1'>Condom</option>
                            <option value='2'>OCP</option>
                            <option value='3'>Injectable</option>
                            <option value='4'>IUCD</option>
                            <option value='5'>Sterilization/BTL</option>
                            <option value='6'>Other</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>TB Screening</label>
                        <?php

                        echo "<select name='tbs' id='tbs' class='form-control' required>
                        <option value=''>Select TB Screening</option>
                                        <option value='n'>Negative</option>
                                        <option value='p'>Positive</option>
                                       </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>STI Screening</label>
                        <?php

                        echo "<select name='stis' id='stis' class='form-control' required>
                        <option value=''>Select STI Screening</option>
                                        <option value='n'>Negative</option>
                                        <option value='p'>Positive</option>
                                       </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Cervical Cancer Screening</label>
                        <?php

                        echo "<select name='via' id='via' class='form-control' required>
                        <option value=''>Select VIA Screening</option>
                                        <option value='n'>Negative</option>
                                        <option value='p'>Positive</option>
                                       </select>";
                        ?>
                    </div>
                </div>
                <!-- <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Pill Count</label>
                        <input type="number" id="pill_count" name="pill_count" class="form-control" required>
                    </div>
                </div> -->

            </div>
            <div class="row">
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>CTX</label>
                        <?php

                        echo "<select name='ctx' id='ctx' class='form-control' required>
                        <option value=''>Select CTX</option>
                                        <option value='yes'>Yes</option>
                                        <option value='no'>No</option>
                                       </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>IPT</label>
                        <?php

                        echo "<select name='ipt' id='ipt' class='form-control' required>
                        <option value=''>Select IPT</option>
                                        <option value='yes'>Yes</option>
                                        <option value='no'>No</option>
                                       </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Anti TB Rx</label>
                        <?php

                        echo "<select name='anti_tb_rx' id='anti_tb_rx' class='form-control' required>
                        <option value=''>Select Antu TB Rx</option>
                                        <option value='yes'>Yes</option>
                                        <option value='no'>No</option>
                                       </select>";
                        ?>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>ART Regimen</label>
                        <input type="text" id="art_regimen" name="art_regimen" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Comments</label>
                        <input type="text" id="comments" name="comments" class="form-control" required>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>ART Care Model</label>
                        <select name='art_care_model' id='art_care_model' style="width: 100%" class="form-control" required>
                            <option value=''>Select ART Care Model</option>
                            <option value='MS'>MS</option>
                            <option value='FT'>FT</option>
                            <option value='CAG'>CAG</option>
                            <option value='TC'>TC</option>
                            <option value='OM'>OM</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1 aaa">
                    <div class="form-group">
                        <label>ADULT ART</label>
                        <select name='adult_art' id='adult_art' style="width: 100%; display: block;" class="form-control">
                            <option value='' onclick="toggle_show('ped_art')">Select Adult ART Regimen</option>
                            <option value='TDF+3TC+DTG' onclick="toggle_hide('ped_art')">TDF+3TC+DTG</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('ped_art')">TDF+3TC+EFV</option>
                            <option value="AZT+3TC+DTG" onclick="toggle_hide('ped_art')">AZT+3TC+DTG</option>
                            <option value="AZT+3TC+EFV" onclick="toggle_hide('ped_art')">AZT+3TC+EFV</option>
                            <option value="ABC+3TC+DTG" onclick="toggle_hide('ped_art')">ABC+3TC+DTG</option>
                            <option value="AZT+3TC+NVP" onclick="toggle_hide('ped_art')">AZT+3TC+NVP</option>
                            <option value="TDF+3TC+NVP" onclick="toggle_hide('ped_art')">TDF+3TC+NVP</option>
                            <option value="ABC+3TC+NVP" onclick="toggle_hide('ped_art')">ABC+3TC+NVP</option>
                        </select>
                        <span class="errorr"></span>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>PEDIATRIC ART</label>
                        <select name='ped_art' id='ped_art' style="width: 100%; display: block;" class="form-control bbb">
                            <option value='' onclick="toggle_show('adult_art')">Select Pediatric ART Regimen</option>
                            <option value="ABC+3TC+LPV/r" onclick="toggle_hide('adult_art')">ABC+3TC+LPV/r</option>
                            <option value="ABC+3TC+EFV" onclick="toggle_hide('adult_art')">ABC+3TC+EFV</option>
                            <option value="TDF+3TC+DTG" onclick="toggle_hide('adult_art')">TDF+3TC+DTG</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('adult_art')">TDF+3TC+EFV</option>
                            <option value="AZT+3TC+LPV/r" onclick="toggle_hide('adult_art')">AZT+3TC+LPV/r</option>
                            <option value="AZT+3TC+NVP" onclick="toggle_hide('adult_art')">AZT+3TC+NVP</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('adult_art')">TDF+3TC+EFV</option>
                        </select>
                        <span class="errorr"></span>
                    </div>
                </div>

                <script >
                    function toggle_hide(id) {
                        var e = document.getElementById(id);
                        //if(e.style.display == 'block')
                        //e.style.display = 'none';
                        if (e.style.display == 'block')
                            e.style.display = 'none';

                    }

                    function toggle_show(id) {
                        var e = document.getElementById(id);
                        //if(e.style.display == 'block')
                        //e.style.display = 'block';
                        if (e.style.display == 'none')
                            e.style.display = 'block';

                    }
                </script>


            </div>
            <div class="row">
                <div class="col-md-6 pl-1">
                    <div class="form-group">
                        <label>Visit Date</label>
                        <input type="date" id="visit_date" name="visit_date" class="form-control" required>

                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Next Date</label>
                        <input type="date" id="next_date" name="next_date" class="form-control" required>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <button type="submit" name="artrefill" id="artrefill" class="btn btn-primary btn-round">Submit</button>
                </div>
            </div>
        </form>
    </div>