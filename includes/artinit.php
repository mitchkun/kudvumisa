<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h5 class="card-title">ART INITIATION VISIT</h5>
    </div>
    <?php if(isset($bp_dia)) { ?>
    <div class="card-body">
        <form action="functions/artinit_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">

                <div class="col-md-5 pr-1">
                    <div class="form-group">
                        <label>Weight/Height</label>
                        <input type="text" id="weight" name="weight" class="form-control" value="<?php echo round(($weight / $height), 2); ?>">
                        <input type="hidden" name="bp_dia" value="<?php echo $bp_dia; ?>"/>
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>BMI</label>
                        <input type="text" id="bmi" name="bmi" class="form-control" value="<?php echo round(($weight / (($height/100) * ($height/100))), 2);
                                                                                            $bmi = round(($weight / (($height/100) * ($height/100))), 2); ?>">
                                                                                            <input type="hidden" name="bp_sys" value="<?php  {echo $bp_sys;}  ?>"/>
                    </div>
                </div>
                
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label for="exampleInputEmail1">MUAC</label>
                        <input type="text" id="muac" name="muac" class="form-control">
                        <input type="hidden" id="vitals_id" name="vitals_id" class="form-control" value="<?php echo $vitalsid; ?>">
                    </div>
                </div>
                
            </div>
            <div class="row">

                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Nutritional Status</label>
                        <?php

                        echo "<select name='ns' id='ns' class='form-control'>
                        <option value=''>Select</option>
                                        <option value='Normal'>Normal</option>
                                        <option value='Mild'>Mild</option>
                                        <option value='Moderate'>Moderate</option>
                                        <option value='Severe'>Severe</option>
                                        </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Functional Status</label>
                        <?php

                        echo "<select name='fs' id='fs' class='form-control'>
                        <option value=''>Select</option>
                                        <option value='Working'>Working</option>
                                        <option value='Ambulating'>Ambulating</option>
                                        <option value='Bedridden'>Bedridden</option>
                                        </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>TB Screening</label>
                        <?php

                        echo "<select name='tbs' id='tbs' class='form-control'>
                        <option value=''>Select</option>
                                        <option value='Negative'>Negative</option>
                                        <option value='Positive'>Positive</option>
                                       </select>";
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
            <div class="row">

            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>WHO Stage</label>

                        <?php

                        echo "<select name='who' id='who' class='form-control' required>
                                        <option value=''>Select Stage</option>
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        </select>";
                        ?>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>CD4 Count</label>
                        <input type="text" id="cd4" name="cd4" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Hb</label>
                        <input type="number" id="hb" name="hb" class="form-control">
                        <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                echo $nstart; ?>" hidden="true">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>ALT Results</label>
                        <input type="text" id="alt" name="alt" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>AST Results</label>
                        <input type="text" id="ast" name="ast" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>CREAT Results</label>
                        <input type="number" id="creat" name="creat" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row"><label style="margin: 10px">Recent Ol Hx</label><textarea name="message" rows="3" style="margin: 10px" class="form-control col-md-15 pl-1"></textarea></div>
            <div class="row">
                <label style="margin: 10px">Obstetric Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Parity</label>
                        <input type="text" id="parity" name="parity" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>PMTCT</label><br />
                        <select name='pmtct' id='pmtct' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Regimen</label>
                        <input type="text" id="regimen" name="regimen" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Pregnant</label>
                        <select name='pregnant' id='pregnant' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>LMP</label><br />
                        <input type="text" id="lmp" name="lmp" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Trimester</label>
                        <select name='trimester' id='trimester' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='1st'>1st</option>
                            <option value='2nd'>2nd</option>
                            <option value='3rd'>3rd</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>EDD</label>
                        <input type="text" id="edd" name="edd" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>FP</label>
                        <select name='fp' id='fp' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='0'>None</option>
                            <option value='1'>Condom</option>
                            <option value='2'>OCP</option>
                            <option value='3'>Injectable</option>
                            <option value='4'>IUCD</option>
                            <option value='5'>Sterilization/BTL</option>
                            <option value='6'>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>HIV Exposed Infant Prophylaxis</label>
                        <select name='prophylaxis' id='prophylaxis' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='[eIP(NVP+AZT+-NVP)]'>[eIP(NVP+AZT+-NVP)]</option>
                            <option value='[6 weeks NVP]'>[6 weeks NVP]</option>
                            <option value='[Extended NVP]'>[Extended NVP]</option>
                            <option value='[SDNVP]'>[SDNVP]</option>
                            <option value='[None]'>[None]</option>
                            <option value='[Unknown]'>[Unknown]</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Drug Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Past ART</label>
                        <select name='past_art' id='past_art' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Date</label><br />
                        <input type="date" id="art_date" name="art_date" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Regimen</label>
                        <input type="text" id="art_regimen" name="art_regimen" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>On CTX</label>
                        <select name='on_ctx' id='on_ctx' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Date</label><br />
                        <input type="date" id="ctx_date" name="ctx_date" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Current Medications 1</label>
                        <input type="text" id="current_med1" name="current_med1" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Current Medications 2</label><br />
                        <input type="text" id="current_med2" name="current_med2" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Current Medications 3</label><br />
                        <input type="text" id="current_med3" name="current_med3" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Social Hx</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Smoking</label>
                        <select name='smoking' id='smoking' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Alcohol</label><br />
                        <select name='alcohol' id='alcohol' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Consistent Condom Use</label>
                        <select name='ccu' id='ccu' style="width: 100%" class="form-control">
                            <option value=''>Select</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='N/A'>N/A</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-md-2 pl-1" style="margin: 10px">Family HTC Hx:</label>
                <label class="col-md-2 pl-1">Age</label>
                <label class="col-md-3 pl-1">HIV Status</label>
                <label class="col-md-3 pl-1">On ART</label>
            </div>
            <div class="row">
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                        <select name='spouse_relation' id='spouse_relation' style="width: 100%" class="form-control">
                            <option value=''>Select</option>
                            <option value='Partner'>Partner</option>
                            <option value='Child'>Child</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 px-1">
                    <div class="form-group">
                        <input type="text" id="spouse_age" name="spouse_age" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="spouse_hiv" name="spouse_hiv" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="spouse_on_art" name="spouse_on_art" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                    <select name='child1_relation' id='child1_relation' style="width: 100%" class="form-control">
                            <option value=''>Select</option>
                            <option value='Partner'>Partner</option>
                            <option value='Child'>Child</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 px-1">
                    <div class="form-group">
                        <input type="text" id="child1_age" name="child1_age" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="child1_hiv" name="child1_hiv" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="child1_on_art" name="child1_on_art" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 pr-1">
                    <div class="form-group">
                    <select name='child2_relation' id='child2_relation' style="width: 100%" class="form-control">
                            <option value=''>Select</option>
                            <option value='Partner'>Partner</option>
                            <option value='Child'>Child</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 px-1">
                    <div class="form-group">
                        <input type="text" id="child2_age" name="child2_age" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="child2_hiv" name="child2_hiv" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <input type="text" id="child2_on_art" name="child2_on_art" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Examination: General</label>
            </div>
            <div class="row">
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Head/neck</label>
                        <input type="text" id="head_neck" name="head_neck" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>Respiratory</label><br />
                        <input type="text" id="respiratory" name="respiratory" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>GIT</label>
                        <input type="text" id="git" name="git" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>CNS</label>
                        <input type="text" id="cns" name="cns" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Examination: Muscular Skeletal</label>
            </div>
            <div class="row">
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>MSS</label>
                        <input type="text" id="mss" name="mss" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>CVS</label><br />
                        <input type="text" id="cvs" name="cvs" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>UGS</label>
                        <input type="text" id="ugs" name="ugs" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>Skin</label>
                        <input type="text" id="skin" name="skin" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row"><label style="margin: 10px">Associated Dx</label><input name="associate_dx" rows="2" style="margin: 10px" class="form-control col-md-15 pl-1"></input></div>
            <div class="row">
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>ART Care Model</label>
                        <select name='art_care_model' id='art_care_model' style="width: 100%" class="form-control" required>
                            <option value=''>Select ART Care Model</option>
                            <option value='MS'>MS</option>
                            <option value='FT'>FT</option>
                            <option value='CAG'>CAG</option>
                            <option value='TC'>TC</option>
                            <option value='OM'>OM</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pl-1">
                    <div class="form-group">
                        <label>Anti TB Rx</label>
                        <?php

                        echo "<select name='anti_tb_rx' id='anti_tb_rx' class='form-control' required>
                        <option value=''>Select Antu TB Rx</option>
                                        <option value='yes'>Yes</option>
                                        <option value='no'>No</option>
                                       </select>";
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">ART - REGIMEN:</label>
            </div>
            <div class="row">
            <div class="col-md-6 pr-1 aaa">
                    <div class="form-group">
                        <label>ADULT ART</label>
                        <select name='adult_art' id='adult_art' style="width: 100%; display: block;" class="form-control">
                            <option value='' onclick="toggle_show('ped_art')">Select Adult ART Regimen</option>
                            <option value='TDF+3TC+DTG' onclick="toggle_hide('ped_art')">TDF+3TC+DTG</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('ped_art')">TDF+3TC+EFV</option>
                            <option value="AZT+3TC+DTG" onclick="toggle_hide('ped_art')">AZT+3TC+DTG</option>
                            <option value="AZT+3TC+EFV" onclick="toggle_hide('ped_art')">AZT+3TC+EFV</option>
                            <option value="ABC+3TC+DTG" onclick="toggle_hide('ped_art')">ABC+3TC+DTG</option>
                            <option value="AZT+3TC+NVP" onclick="toggle_hide('ped_art')">AZT+3TC+NVP</option>
                            <option value="TDF+3TC+NVP" onclick="toggle_hide('ped_art')">TDF+3TC+NVP</option>
                            <option value="ABC+3TC+NVP" onclick="toggle_hide('ped_art')">ABC+3TC+NVP</option>
                        </select>
                        <span class="errorr"></span>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>PEDIATRIC ART</label>
                        <select name='ped_art' id='ped_art' style="width: 100%; display: block;" class="form-control bbb">
                            <option value='' onclick="toggle_show('adult_art')">Select Pediatric ART Regimen</option>
                            <option value="ABC+3TC+LPV/r" onclick="toggle_hide('adult_art')">ABC+3TC+LPV/r</option>
                            <option value="ABC+3TC+EFV" onclick="toggle_hide('adult_art')">ABC+3TC+EFV</option>
                            <option value="TDF+3TC+DTG" onclick="toggle_hide('adult_art')">TDF+3TC+DTG</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('adult_art')">TDF+3TC+EFV</option>
                            <option value="AZT+3TC+LPV/r" onclick="toggle_hide('adult_art')">AZT+3TC+LPV/r</option>
                            <option value="AZT+3TC+NVP" onclick="toggle_hide('adult_art')">AZT+3TC+NVP</option>
                            <option value="TDF+3TC+EFV" onclick="toggle_hide('adult_art')">TDF+3TC+EFV</option>
                        </select>
                        <span class="errorr"></span>
                    </div>
                </div>
                <script >
                    function toggle_hide(id) {
                        var e = document.getElementById(id);
                        //if(e.style.display == 'block')
                        //e.style.display = 'none';
                        if (e.style.display == 'block')
                            e.style.display = 'none';

                    }

                    function toggle_show(id) {
                        var e = document.getElementById(id);
                        //if(e.style.display == 'block')
                        //e.style.display = 'block';
                        if (e.style.display == 'none')
                            e.style.display = 'block';

                    }
                </script>


            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Food Supplement</label>
                        <select name='food_supp' id='food_supp' style="width: 100%" class="form-control">
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Return Date</label><br />
                        <input type="date" id="return_date" name="return_date" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Care Entry Point</label>
                        <select name='cep' id='cep' style="width: 100%" class="form-control">
                        <option value=''>Select</option>
                            <option value='VCT'>VCT</option>
                            <option value='HTC'>HTC</option>
                            <option value='PMTCT'>PMTCT</option>
                            <option value='TB'>TB</option>
                            <option value='STI'>STI</option>
                            <option value='U5 Clinic'>U5 CliniC</option>
                            <option value='DBS'>DBS</option>
                            <option value='OPD'>OPD</option>
                            <option value='WARD'>WARD</option>
                            <option value='Pre-ART'>Pre-ART</option>
                            <option value='OUTREACH'>OUTREACH</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Other (Specify)</label><br />
                        <input type="text" id="cep_other" name="cep_other" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="update ml-auto mr-auto">
                    <button type="submit" name="artinitiate" id="artinitiate" class="btn btn-primary btn-round">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <?php } else{
                            echo "<h2>Take Vitals</h2><br>";
                        } ?>