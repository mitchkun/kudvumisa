<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">OPD</h2>
    </div>
    <form action="functions/opdfn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
        <div class="card-body">

            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Complaint</label>
                        <textarea type="text" id="complaint" name="complaint" rows="5" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h2 style="margin: 10px">System Review</h2>
            </div>
            <div class="row">
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>Head/neck</label>
                        <input type="text" id="head_neck" name="head_neck" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>Respiratory</label><br />
                        <input type="text" id="respiratory" name="respiratory" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>GIT</label>
                        <input type="text" id="git" name="git" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>CNS</label>
                        <input type="text" id="cns" name="cns" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <label style="margin: 10px">Examination: Muscular Skeletal</label>
            </div>
            <div class="row">
                <div class="col-md-3 pr-1">
                    <div class="form-group">
                        <label>MSS</label>
                        <input type="text" id="mss" name="mss" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 px-1">
                    <div class="form-group">
                        <label>CVS</label><br />
                        <input type="text" id="cvs" name="cvs" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>UGS</label>
                        <input type="text" id="ugs" name="ugs" class="form-control">
                    </div>
                </div>
                <div class="col-md-3 pl-1">
                    <div class="form-group">
                        <label>Skin</label>
                        <input type="text" id="skin" name="skin" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <h3 style="margin: 10px">DIAGNOSIS</h3>
                <a href="#" onclick="myFunction()" id="insert-more"> Add New Row </a>
                <script>
                    function myFunction() {
                        var btn = document.createElement("DIV");
                        btn.innerHTML = document.getElementById('dx'); //"CLICK ME";
                        document.body.appendChild(btn);
                    }
                </script>
            </div>
            <div class="row" id="dx">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <select name='opddx[]' style="width: 100%" class="form-control" required>
                            <option value=''>Select Diagnosis 1</option>
                            <option value='Pregnant'>Pregnant</option>
                            <option value='Abortion'>Abortion</option>
                            <option value='Fertility Problems'>Fertility Problems</option>

                            <option value='Viral Hemorrhagic Fever'>Viral Hemorrhagic Fever</option>
                            <option value='Typhoid Fever'>Typhoid Fever</option>
                            <option value='Plague'>Plague</option>
                            <option value='Yellow Fever'>Yellow Fever</option>
                            <option value='Neonatal Tetanus'>Neonatal Tetanus</option>
                            <option value='Meningitis'>Meningitis</option>
                            <option value='Suspected Measles'>Suspected Measles</option>
                            <option value='Acute Flaccid Paralysis'>Acute Flaccid Paralysis</option>
                            <option value='Rabies'>Rabies</option>
                            <option value='Suspected Cholera'>Suspected Cholera</option>

                            <option value='Persistent Diarrhoea'>Persistent Diarrhoea</option>
                            <option value='Acute Watery Diarrhoea'>Acute Watery Diarrhoea</option>
                            <option value='Dysentery'>Dysentery</option>
                            <option value='Diarrhoea With Bloody Dysentery'>Diarrhoea With Bloody Dysentery</option>
                            <option value='Diarrhoea With Some Dehydration'>Diarrhoea With Some Dehydration</option>
                            <option value='Diarrhoea With Severe Dehydration'>Diarrhoea With Severe Dehydration</option>

                            <option value='Suspected Pulmonary Tuberculosis'>Suspected Pulmonary Tuberculosis</option>
                            <option value='Upper Respiratory Infection'>Upper Respiratory Infection</option>
                            <option value='Lower Respiratory Infection (severe)'>Lower Respiratory Infection (severe)</option>
                            <option value='Lower Respiratory Infection (mild)'>Lower Respiratory Infection (mild)</option>
                            <option value='Pneumonia (mild)'>Pneumonia (mild)</option>
                            <option value='Pneumonia (severe)'>Pneumonia (severe)</option>

                            <option value='Intestinal Worms'>Intestinal Worms</option>
                            <option value='Malaria  (severe aneamia)'>Malaria (severe aneamia)</option>
                            <option value='Malaria  (uncomplicated)'>Malaria (uncomplicated)</option>
                            <option value='Chicken Pox'>Chicken Pox</option>
                            <option value='Mumps'>Mumps</option>
                            <option value='Herpes Zorster'>Herpes Zorster</option>
                            <option value='Leprosy'>Leprosy</option>

                            <option value='Under Weight'>Under Weight</option>
                            <option value='Over Weight'>Over Weight</option>
                            <option value='Obesity'>Obesity</option>
                            <option value='Diabetes Mellitus'>Diabetes Mellitus</option>
                            <option value='Goitre'>Goitre</option>

                            <option value='Hypertension'>Hypertension</option>
                            <option value='Digestive Disorders'>Digestive Disorders</option>
                            <option value='Epilepsy'>Epilepsy</option>
                            <option value='Severe Dehydration'>Severe Dehydration</option>
                            <option value='Dehydration'>Dehydration</option>
                            <option value='Musculo Skeletal Conditions'>Musculo Skeletal Conditions</option>
                            <option value='Injury'>Injury</option>
                            <option value='Road Traffic Accident'>Road Traffic Accident</option>
                            <option value='Cardiac Disease'>Cardiac Disease</option>
                            <option value='Allergic Reaction'>Allergic Reaction</option>
                            <option value='Skin disorders'>Skin disorders</option>
                            <option value='Eye Disease'>Eye Disease</option>
                            <option value='Ear Problems'>Ear Problems</option>
                            <option value='Other'>Other</option>
                            <option value='Gastric Ulcers'>Gastric Ulcers</option>
                            <option value='Dental Caries'>Dental Caries</option>
                            <option value='Oral Health Problems'>Oral Health Problems</option>
                            <option value='Vaginal Discharge'>Vaginal Discharge</option>
                            <option value='Urethral Discharge'>Urethral Discharge</option>
                            <option value='Genital Ulcer'>Genital Ulcer</option>
                            <option value='Other STI'>Other STI</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <select name='opddx[]' style="width: 100%" class="form-control" required>
                            <option value=''>Select Diagnosis 2</option>
                            <option value='Pregnant'>Pregnant</option>
                            <option value='Abortion'>Abortion</option>
                            <option value='Fertility Problems'>Fertility Problems</option>

                            <option value='Viral Hemorrhagic Fever'>Viral Hemorrhagic Fever</option>
                            <option value='Typhoid Fever'>Typhoid Fever</option>
                            <option value='Plague'>Plague</option>
                            <option value='Yellow Fever'>Yellow Fever</option>
                            <option value='Neonatal Tetanus'>Neonatal Tetanus</option>
                            <option value='Meningitis'>Meningitis</option>
                            <option value='Suspected Measles'>Suspected Measles</option>
                            <option value='Acute Flaccid Paralysis'>Acute Flaccid Paralysis</option>
                            <option value='Rabies'>Rabies</option>
                            <option value='Suspected Cholera'>Suspected Cholera</option>

                            <option value='Persistent Diarrhoea'>Persistent Diarrhoea</option>
                            <option value='Acute Watery Diarrhoea'>Acute Watery Diarrhoea</option>
                            <option value='Dysentery'>Dysentery</option>
                            <option value='Diarrhoea With Bloody Dysentery'>Diarrhoea With Bloody Dysentery</option>
                            <option value='Diarrhoea With Some Dehydration'>Diarrhoea With Some Dehydration</option>
                            <option value='Diarrhoea With Severe Dehydration'>Diarrhoea With Severe Dehydration</option>

                            <option value='Suspected Pulmonary Tuberculosis'>Suspected Pulmonary Tuberculosis</option>
                            <option value='Upper Respiratory Infection'>Upper Respiratory Infection</option>
                            <option value='Lower Respiratory Infection (severe)'>Lower Respiratory Infection (severe)</option>
                            <option value='Lower Respiratory Infection (mild)'>Lower Respiratory Infection (mild)</option>
                            <option value='Pneumonia (mild)'>Pneumonia (mild)</option>
                            <option value='Pneumonia (severe)'>Pneumonia (severe)</option>

                            <option value='Intestinal Worms'>Intestinal Worms</option>
                            <option value='Malaria  (severe aneamia)'>Malaria (severe aneamia)</option>
                            <option value='Malaria  (uncomplicated)'>Malaria (uncomplicated)</option>
                            <option value='Chicken Pox'>Chicken Pox</option>
                            <option value='Mumps'>Mumps</option>
                            <option value='Herpes Zorster'>Herpes Zorster</option>
                            <option value='Leprosy'>Leprosy</option>

                            <option value='Under Weight'>Under Weight</option>
                            <option value='Over Weight'>Over Weight</option>
                            <option value='Obesity'>Obesity</option>
                            <option value='Diabetes Mellitus'>Diabetes Mellitus</option>
                            <option value='Goitre'>Goitre</option>

                            <option value='Hypertension'>Hypertension</option>
                            <option value='Digestive Disorders'>Digestive Disorders</option>
                            <option value='Epilepsy'>Epilepsy</option>
                            <option value='Severe Dehydration'>Severe Dehydration</option>
                            <option value='Dehydration'>Dehydration</option>
                            <option value='Musculo Skeletal Conditions'>Musculo Skeletal Conditions</option>
                            <option value='Injury'>Injury</option>
                            <option value='Road Traffic Accident'>Road Traffic Accident</option>
                            <option value='Cardiac Disease'>Cardiac Disease</option>
                            <option value='Allergic Reaction'>Allergic Reaction</option>
                            <option value='Skin disorders'>Skin disorders</option>
                            <option value='Eye Disease'>Eye Disease</option>
                            <option value='Ear Problems'>Ear Problems</option>
                            <option value='Other'>Other</option>
                            <option value='Gastric Ulcers'>Gastric Ulcers</option>
                            <option value='Dental Caries'>Dental Caries</option>
                            <option value='Oral Health Problems'>Oral Health Problems</option>
                            <option value='Vaginal Discharge'>Vaginal Discharge</option>
                            <option value='Urethral Discharge'>Urethral Discharge</option>
                            <option value='Genital Ulcer'>Genital Ulcer</option>
                            <option value='Other STI'>Other STI</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <select name='opddx[]' style="width: 100%" class="form-control" required>
                            <option value=''>Select Diagnosis 3</option>
                            <option value='Pregnant'>Pregnant</option>
                            <option value='Abortion'>Abortion</option>
                            <option value='Fertility Problems'>Fertility Problems</option>

                            <option value='Viral Hemorrhagic Fever'>Viral Hemorrhagic Fever</option>
                            <option value='Typhoid Fever'>Typhoid Fever</option>
                            <option value='Plague'>Plague</option>
                            <option value='Yellow Fever'>Yellow Fever</option>
                            <option value='Neonatal Tetanus'>Neonatal Tetanus</option>
                            <option value='Meningitis'>Meningitis</option>
                            <option value='Suspected Measles'>Suspected Measles</option>
                            <option value='Acute Flaccid Paralysis'>Acute Flaccid Paralysis</option>
                            <option value='Rabies'>Rabies</option>
                            <option value='Suspected Cholera'>Suspected Cholera</option>

                            <option value='Persistent Diarrhoea'>Persistent Diarrhoea</option>
                            <option value='Acute Watery Diarrhoea'>Acute Watery Diarrhoea</option>
                            <option value='Dysentery'>Dysentery</option>
                            <option value='Diarrhoea With Bloody Dysentery'>Diarrhoea With Bloody Dysentery</option>
                            <option value='Diarrhoea With Some Dehydration'>Diarrhoea With Some Dehydration</option>
                            <option value='Diarrhoea With Severe Dehydration'>Diarrhoea With Severe Dehydration</option>

                            <option value='Suspected Pulmonary Tuberculosis'>Suspected Pulmonary Tuberculosis</option>
                            <option value='Upper Respiratory Infection'>Upper Respiratory Infection</option>
                            <option value='Lower Respiratory Infection (severe)'>Lower Respiratory Infection (severe)</option>
                            <option value='Lower Respiratory Infection (mild)'>Lower Respiratory Infection (mild)</option>
                            <option value='Pneumonia (mild)'>Pneumonia (mild)</option>
                            <option value='Pneumonia (severe)'>Pneumonia (severe)</option>

                            <option value='Intestinal Worms'>Intestinal Worms</option>
                            <option value='Malaria  (severe aneamia)'>Malaria (severe aneamia)</option>
                            <option value='Malaria  (uncomplicated)'>Malaria (uncomplicated)</option>
                            <option value='Chicken Pox'>Chicken Pox</option>
                            <option value='Mumps'>Mumps</option>
                            <option value='Herpes Zorster'>Herpes Zorster</option>
                            <option value='Leprosy'>Leprosy</option>

                            <option value='Under Weight'>Under Weight</option>
                            <option value='Over Weight'>Over Weight</option>
                            <option value='Obesity'>Obesity</option>
                            <option value='Diabetes Mellitus'>Diabetes Mellitus</option>
                            <option value='Goitre'>Goitre</option>

                            <option value='Hypertension'>Hypertension</option>
                            <option value='Digestive Disorders'>Digestive Disorders</option>
                            <option value='Epilepsy'>Epilepsy</option>
                            <option value='Severe Dehydration'>Severe Dehydration</option>
                            <option value='Dehydration'>Dehydration</option>
                            <option value='Musculo Skeletal Conditions'>Musculo Skeletal Conditions</option>
                            <option value='Injury'>Injury</option>
                            <option value='Road Traffic Accident'>Road Traffic Accident</option>
                            <option value='Cardiac Disease'>Cardiac Disease</option>
                            <option value='Allergic Reaction'>Allergic Reaction</option>
                            <option value='Skin disorders'>Skin disorders</option>
                            <option value='Eye Disease'>Eye Disease</option>
                            <option value='Ear Problems'>Ear Problems</option>
                            <option value='Other'>Other</option>
                            <option value='Gastric Ulcers'>Gastric Ulcers</option>
                            <option value='Dental Caries'>Dental Caries</option>
                            <option value='Oral Health Problems'>Oral Health Problems</option>
                            <option value='Vaginal Discharge'>Vaginal Discharge</option>
                            <option value='Urethral Discharge'>Urethral Discharge</option>
                            <option value='Genital Ulcer'>Genital Ulcer</option>
                            <option value='Other STI'>Other STI</option>
                        </select>
                    </div>
                </div>


            </div>
            <hr>


            <div class="col-md-12 pl-1">
                <div class="form-group">
                    <div class="form-group">
                        <label>NON PHARM MANAGMENT</label>
                        <textarea type="text" id="nonpharm" name="nonpharm" rows="3" class="form-control"></textarea>
                    </div>

                </div>
            </div>
            <div class="col-md-12 pl-1">
                <div class="form-group">

                    <label>Referral</label>
                    <textarea type="text" id="referral" name="referral" rows="3" class="form-control"></textarea>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="update ml-auto mr-auto">
                <button type="submit" name="opd" id="opd" class="btn btn-primary btn-round">Submit</button>
            </div>
        </div>
    </form>
</div>