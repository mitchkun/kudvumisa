<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">Confirmation Register : <?php echo date("Y-m-d"); ?></h2>
    </div>
    <div class="card-body">
        <form action="functions/artinit_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of 1st </label>
                        <br>
                        <input type="date" id="date_hiv_test" name="date_hiv_test" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Name of Unit/Facility (that conducted the first test)</label>
                        <input type="text" id="unit_facility" name="unit_facility" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date client retested </label>
                        <br>
                        <input type="date" id="date_hiv_retest" name="date_hiv_retest" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Name of second tester</label>
                        <input type="text" id="2nd_tester" name="2nd_tester" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <label>Re-testing Results (choose where applicable</label>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Determine</label>
                        <br>
                        <select id='determine_result' name='determine_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>Reactive</option>
                            <option value='NR'>Non-Reactive</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Unigold</label>
                        <select id='unigold_result' name='unigold_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>Reactive</option>
                            <option value='NR'>Non-Reactive</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Clearview</label>
                        <select id='clearview_result' name='clearview_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>Reactive</option>
                            <option value='NR'>Non-Reactive</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>HIV Retest Result</label>
                        <br>
                        <select id='hiv_retest_result' name='hiv_retest_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>Reactive</option>
                            <option value='NR'>Non-Reactive</option>
                            <option value='INC'>Inconclusive</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Number of HIV self test kits</label><br>
                        <input type="checkbox" id="hiv_care" name="hiv_care" value="Enrolled into HIV care (not ART)">
                        <label for="hiv_care">Enrolled into HIV care (not ART)</label><br>
                        <input type="checkbox" id="init_art" name="init_art" value="Initiated ART">
                        <label for="init_art">Initiated ART</label><br>
                        <input type="checkbox" id="blood_sample" name="blood_sample" value="Blood Sample taken to confirm HIV positive status (DNA PCR)">
                        <label for="blood_sample">Blood Sample taken to confirm HIV positive status (DNA PCR)</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>DNA PCR Results</label>
                        <br>
                        <select id='dna_pcr_results' name='dna_pcr_results' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='DNA PCR POS'>DNA PCR POSITIVE</option>
                            <option value='DNA PCR NEG'>DNA PCR NEGATIVE</option>
                            <option value='NOT APPLICABLE'>NOT APPLICABLE</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Date DNA PCR Results given to Client</label>
                        <input type="date" id="date_results_given" name="date_results_given" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Post DNA PCR follow up action (tick where applicable)</label>
                        <select id='post_dnapcr_fup' name='post_dnapcr_fup' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Enrolled into HIV care (not ART)'>Enrolled into HIV care (not ART)</option>
                            <option value='Initiated ART'>Initiated ART</option>
                            <option value='Other'>Other (please specify)</option>
                        </select>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        <label>Comments</label>
                        <input type="text" id="comments" name="comments" class="form-control">
                    </div>
                </div>
            </div>


    </div>

    <div class="row">
        <div class="update ml-auto mr-auto">
            <button type="submit" name="link_register" id="link_register" class="btn btn-primary btn-round">Submit</button>
        </div>
    </div>
    </form>
</div>