<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">Linkages Case Management Register : <?php echo date("Y-m-d"); ?></h2>
    </div>
    <div class="card-body">
        <form action="functions/link_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date Tested</label>
                        <br>
                        <input type="date" id="date_tested" name="date_tested" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of retesting for verification</label>
                        <input type="date" id="date_retest" name="date_retest" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Referral Form Number</label>
                        <br>
                        <input type="text" id="ref_form" name="ref_form" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Transfer in Date</label>
                        <input type="date" id="trans_date" name="trans_date" class="form-control">
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Referral From</label>
                        <select id='ref_from' name='ref_from' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='1'>Community</option>
                            <option value='2'>Facility</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">

                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>ART Number</label>
                        <br>
                        <input type="text" id="art_num" name="art_num" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Date of ART enrollment</label>
                        <input type="date" id="date_art" name="date_art" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>First Follow up call for both ART/Pre-ART</label>
                        <br>
                        <input type="date" id="first_fup" name="first_fup" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>1st Call Day 3-5</label>
                        <input type="date" id="first_fup_call" name="first_fup_call" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Second Follow up call for both ART/Pre-ART</label>
                        <br>
                        <input type="date" id="second_fup" name="second_fup" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>2nd follow up Call Day 10-12</label>
                        <input type="date" id="second_fup_call" name="second_fup_call" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>1st ART refill / Pre ART clinical Visit</label>
                        <br>
                        <input type="date" id="first_art_refill" name="first_art_refill" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Day 14</label>
                        <input type="date" id="first_art_refill_fup" name="first_art_refill_fup" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Third Follow up call for both ART/Pre-ART</label>
                        <br>
                        <input type="date" id="third_fup" name="third_fup" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>3rd follow up Call Day 21-29</label>
                        <input type="date" id="third_fup_call" name="third_fup_call" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>2nd ART refill / Pre ART clinical Visit</label>
                        <br>
                        <input type="date" id="second_art_refill" name="second_art_refill" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Day 30-45</label>
                        <input type="date" id="second_art_refill_fup" name="second_art_refill_fup" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Fourth Follow up call for both ART/Pre-ART</label>
                        <br>
                        <input type="date" id="fourth_fup" name="fourth_fup" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>4th Call Day 44-60</label>
                        <input type="date" id="fourth_fup_call" name="fourth_fup_call" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>3rd ART refill / Pre ART clinical Visit</label>
                        <br>
                        <input type="date" id="third_art_refill" name="third_art_refill" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Day 60-75</label>
                        <input type="date" id="third_art_refill_fup" name="third_art_refill_fup" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Barriers to ART initiation</label>
                        <br>
                        <select id='barriers_art' name='barriers_art' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Specify Major Barrier</label>
                        <input type="text" id="major_barrier" name="major_barrier" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Disclosed to</label>
                        <br>
                        <select id='disclosed' name='disclosed' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='1'>Sexual Partner</option>
                            <option value='2'>Family</option>
                            <option value='3'>Associate</option>
                            <option value='4'>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Index Contacts Tested by Day 60</label>
                        <select id='index_contacts' name='index_contacts' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Final LCM Outcome</label>
                        <br>
                        <input type="date" id="final_lcm" name="final_lcm" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label> </label>
                        <select id='index_contacts' name='index_contacts' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='1'>Completed</option>
                            <option value='2'>Transferred</option>
                            <option value='3'>Lost to Follow Up</option>
                            <option value='4'>Died</option>
                            <option value='5'>Did not initiate ART</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Any Comments</h4>
            </div>
            <div class="row">
                <div class="col-md-12 pr-1">
                    <div class="form-group">
                        
                        <input type="text" id="comments" name="comments" class="form-control">
                    </div>
                </div>

            </div>
            

    </div>

    <div class="row">
        <div class="update ml-auto mr-auto">
            <button type="submit" name="link_register" id="link_register" class="btn btn-primary btn-round">Submit</button>
        </div>
    </div>
    </form>
</div>