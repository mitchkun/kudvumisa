<div class="card card-user">
    <div class="card-header">
        <h5 class="card-title">Art Prescriptions</h5>
        <?php

        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query2 = "SELECT * FROM `prescription`  WHERE emr_no = $emr AND filled = 0";
        $query_run2 = mysqli_query($conn, $query2);



        ?>
        <div class="table-responsive">
            <table>
                <tr>
                    <th>Date</th>
                    <th>Diagnosis</th>


                    <!-- <th>Temperature</th>
                                <th>Pulse Rate</th>
                                <th>RR</th> -->
                    <th>Treatment</th>
                    <th>Filled Prescription</th>
                </tr>
                <?php

                if (mysqli_num_rows($query_run2) > 0) {
                    while ($row2 = mysqli_fetch_assoc($query_run2)) {
                ?>
                        <tr>
                            <td><?php echo $row2['prescr_date'] ?></td>
                            <td><?php echo $row2['diagnosis'] ?></td>

                            <td><?php echo $row2['medication'] ?></td>
                            <td><?php echo $row2['treatment'] ?></td>
                            <td><?php echo $row2['filled'] ?></td>
                        </tr>
                <?php

                    }
                } else {
                    echo "<tr>No records found</tr>";
                }

                ?>
            </table>
        </div>
    </div>

</div>