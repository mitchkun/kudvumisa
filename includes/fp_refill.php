<div class="card card-user" style="margin-top: 20px">
    <div class="card-header">
        <?php

        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
            echo '<h2>' . $_SESSION['success'] . '</h2>';
            unset($_SESSION['success']);
        }

        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
            echo '<h2>' . $_SESSION['status'] . '</h2>';
            unset($_SESSION['status']);
        }


        ?>
        <h2 class="card-title">Family Planning Follow Up Visit</h2>
    </div>
    <div class="card-body">
        <form action="functions/fp_fn.php?emr_no=<?php echo $emr_no1; ?>" method="POST">
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Return Date</label>
                        <input type="date" id="return_date" name="return_date" class="form-control">
                    </div>
                </div>
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>FP Status</label>
                        <br>
                        <select id='fp_status' name='fp_status' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='New Acceptor'>New Acceptor</option>
                            <option value='Re-attend'>Re-attend</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Fp Method/Quantity</h4>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Method</label>
                        <input type="text" id="fp_method" name="fp_method" class="form-control" >
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" id="fp_quantity" name="fp_quantity" class="form-control" >
                    </div>
                </div>
                
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Condom Quantity</h4>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Male</label>
                        <input type="number" id="male_condom" name="male_condom" class="form-control" >
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Female</label>
                        <input type="number" id="female_condom" name="female_condom" class="form-control" >
                    </div>
                </div>
                
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">Other Services</h4>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Pregnancy Test Result</label>
                        <select id='preg_result' name='preg_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>R</option>
                            <option value='NR'>NR</option>
                            <option value='NA'>NA</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Weight (Kg)</label>
                        <input type="number" id="fp_weight" name="fp_weight" class="form-control" value="<?php if($weight > 1) echo $weight;?>" >
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>BP (Specify)</label>
                        <input type="text" id="fp_bp" name="fp_bp" class="form-control" value="<?php if($BP != '') echo $BP;?>" >
                        <input type="hidden" id="fp_sys" name="fp_sys" class="form-control" value="<?php if($BP != '') echo $bp_sys;?>" >
                        <input type="hidden" id="fp_dia" name="fp_dia" class="form-control" value="<?php if($BP != '') echo $bp_dia;?>" >
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">HIV Status Before Arrival</h4>
            </div>
            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>HIV Status</label>
                        <select id='fp_hiv' name='fp_hiv' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>R</option>
                            <option value='NR'>NR</option>
                            <option value='U'>U</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Date of last Test (if R/NR)</label>
                        <input type="date" id="fp_date_hivtest" name="fp_date_hivtest" class="form-control" >
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <h4 style="margin: 10px">HTC (this visit)</h4>
            </div>
            <div class="row">
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Eligible for Re-testing</label>
                        <select id='fp_restest' name='fp_restest' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 px-1">
                    <div class="form-group">
                        <label>Date Tested</label>
                        <input type="date" id="fp_date_tested" name="fp_date_tested" class="form-control" >
                    </div>
                </div>
                <div class="col-md-4 pr-1">
                    <div class="form-group">
                        <label>Result</label>
                        <select id='fp_retest_result' name='fp_retest_result' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='R'>R</option>
                            <option value='NR'>NR</option>
                            <option value='Inc'>Inc</option>
                        </select>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-6 pr-1">
                    <div class="form-group">
                        <label>Linked to Care and Treatment</label>
                        <select id='fp_linked_ct' name='fp_linked_ct' style="width: 100%" class="form-control">
                            <option value=''>Select option</option>
                            <option value='Yes'>Yes</option>
                            <option value='No'>No</option>
                            <option value='NA'>NA</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 px-1">
                    <div class="form-group">
                        <label>Date Art Initiated</label>
                        <input type="date" id="fp_date_art" name="fp_date_art" class="form-control" >
                    </div>
                </div>
            </div>
    </div>

    <div class="row">
        <div class="update ml-auto mr-auto">
            <button type="submit" name="fp_initiate" id="fp_initiate" class="btn btn-primary btn-round">Submit</button>
        </div>
    </div>
    </form>
</div>