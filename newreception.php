<?php

require "includes/config.php";
require "includes/reception.php";
include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Client Profile</h1>

        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="...">
                                <h5 class="title text-center">New Client</h5>
                            </a>
                            <p class="description text-center">
                                EMR #:
                            </p>
                        </div>

                    </div>
                </div>
                <!-- <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Forward to Department</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#LabsModal'>Lab</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#HTSsModal'>HTS</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#NursesModal'>Nurse</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PharmacysModal'>Pharmacy</button>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="col-md-8">
                <div class="card card-user">
                    <div class="card-header">
                        <?php

                        if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                            echo '<h2>' . $_SESSION['success'] . '</h2>';
                            unset($_SESSION['success']);
                        }

                        if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                            echo '<h2>' . $_SESSION['status'] . '</h2>';
                            unset($_SESSION['status']);
                        }


                        ?>
                        <h5 class="card-title">New Profile</h5>
                    </div>
                    <div class="card-body">
                        <form action="createprofile.php" method="POST">
                            <div class="row">
                                <div class="col-md-5 pr-1">
                                    <div class="form-group">
                                        <label>National ID Number/PIN</label>
                                        <input type="text" id="ID" name="ID" class="form-control"  placeholder="PIN" required >
                                    </div>
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label>Surname</label>
                                        <input type="text" id="Surname" name="Surname" class="form-control" placeholder="Surname" required >
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">First Name</label>
                                        <input type="text" id="Name" name="Name" class="form-control" placeholder="First Name" required >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>DOB</label>
                                        <input type="date" id="DOB" name="DOB" class="form-control" placeholder="09/09/1990" required >
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Sex</label><br />
                                        <?php
                                        if (isset($row['Gender'])) {
                                            if ($row['Gender'] == "Female") {
                                                echo "
                                                <select id='Gender' name='Gender' class='form-control'>
                                                <option id='male' value='Male'>Male</option>                                            
                                                <option id='female' value='Female' selected='true'>Female</option>
                                                </select> ". $row['Gender'];
                                            } else if ($row['Gender'] == "Male") {
                                                echo "<select id='Gender' name='Gender' class='form-control' >
                                                
                                                <option id='male' value='Male' selected='true'>Male</option>                                            
                                                <option id='female' value='Female' >Female</option>
                                                </select> ". $row['Gender'];
                                            } else {
                                                echo "<select id='Gender' name='Gender' required class='form-control'>
                                                <option value=''>Select Gender</option> 
                                                <option id='male' value='Male'>Male</option>                                            
                                                <option id='female' value='Female'>Female</option>
                                                </select>";
                                            }
                                        } else {
                                            echo "<select id='Gender' name='Gender' required class='form-control'>
                                            <option value=''>Select Gender</option> 
                                            <option id='male' value='Male'>Male</option>                                            
                                            <option id='female' value='Female'>Female</option>
                                            </select>";
                                        } ?>
                                        <!-- <input type="radio" id="male" name="gender" value="male">
                                        <label for="male">Male</label>
                                        <input type="radio" id="female" name="gender" value="female" checked="true">
                                        <label for="female">Female</label> -->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Physical Address (Including how to reach the home)</label>
                                        <input type="text" id="address" name="address" class="form-control" placeholder="Home Address" required >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Tel/Cell</label>
                                        <input type="text" id="Contact" name="Contact" class="form-control" placeholder="Enter Contact Number" required >
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Occupation</label>
                                        <input type="text" id="Occupation" name="Occupation" class="form-control" placeholder="Accountant, Builder,..." >
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Telephone Work</label>
                                        <input type="number" id="work_contact" name="work_contact" class="form-control" placeholder="+268 ---- ----" >

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>Marital Status</label><br />
                                        <?php
                                        if (isset($row['Marital'])) {
                                            if ($row['Marital'] == "Single") {
                                                echo "<select id='married' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' selected='true'>Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'] ;
                                            } else if ($row['Marital'] == "Married") {
                                                echo "<select id='married' name='Marital' class='form-control'>
                                                <option value='Married' selected='true'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Widowed") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' Single</option>
                                               
                                                <option value='Widowed' selected='true'>>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Divorced") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced' selected='true'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else if ($row['Marital'] == "Co-habitation") {
                                                echo "<select id='Marital' name='Marital' class='form-control'>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation' selected='true'>Co-habitation</option>
                                                </select> " . $row['Marital'];
                                            } else {
                                                echo "<select id='Marital' name='Marital' required class='form-control'>
                                                <option value=''>Select Marital Status</option>
                                                <option value='Married'>Married</option>
                                               
                                                <option value='Single' >Single</option>
                                               
                                                <option value='Widowed'>Widowed</option>
                                               
                                                <option value='Divorced'>Divorced</option>
                                               
                                                <option value='Co-habitation'>Co-habitation</option>
                                                </select> ";
                                            }
                                        } else {
                                            echo "<select id='Marital' name='Marital' required class='form-control'>
                                            <option value=''>Select Marital Status</option>
                                            <option value='Married'>Married</option>
                                           
                                            <option value='Single' >Single</option>
                                           
                                            <option value='Widowed'>Widowed</option>
                                           
                                            <option value='Divorced'>Divorced</option>
                                           
                                            <option value='Co-habitation'>Co-habitation</option>
                                            </select> ";
                                        } ?>
                                        <!-- <input type="radio" id="married" name="marital" value="married">
                                        <label for="married">Married</label>
                                        <input type="radio" id="single" name="marital" value="single">
                                        <label for="single">Single</label>
                                        <input type="radio" id="widowed" name="marital" value="widowed">
                                        <label for="widowed">Widowed</label>
                                        <input type="radio" id="divorced" name="marital" value="divorced">
                                        <label for="divorced">Divorced</label> -->
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Orphan</label>
                                        <br />
                                        <?php
                                        if (isset($row['Orphan'])) {
                                            if ($row['Orphan'] == "Yes") {
                                                echo  "<select name='Orphan' id='Orphan' class='form-control'>
                                                <option value='No'>No</option><option value='Yes'>Yes</option>
                                                
                                                </select> " . $row['Orphan'] ;
                                            } else if ($row['Orphan'] == "No") {
                                                echo "<select name='Orphan' id='Orphan' class='form-control'>
                                                <option value='No'>No</option><option value='Yes'>Yes</option>
                                                
                                                </select> " . $row['Orphan'];
                                            } else {
                                                echo "<select name='Orphan' id='Orphan' class='form-control'>
                                                <option value='No'>No</option><option value='Yes'>Yes</option>
                                                
                                                </select> ";
                                            }
                                        } else {
                                            echo "<select name='Orphan' id='Orphan' class='form-control'>
                                            <option value='No'>No</option><option value='Yes'>Yes</option>
                                            
                                            </select> ";
                                        } ?>
                                        <!--                                         
                                        <input type="radio" id="yes" name="orphan" value="yes">
                                        <label for="yes">Yes</label>
                                        <input type="radio" id="yes" name="orphan" value="yes">
                                        <label for="no">No</label> -->
                                    </div>
                                </div>&nbsp;&nbsp;
                                <div class="col-md-5 pl-1">
                                    <div class="form-group">
                                        <label>Living Parent (tick appropriate)</label>
                                        <br />
                                        <?php
                                        // if (isset($row['Parent'])) {
                                        //     if ($row['Parent'] == "Mother") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else if ($row['Parent'] == "Father") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else if ($row['Parent'] == "None") {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>" . $row['Parent'];
                                        //     } else {
                                        //         echo  "<select name='Parent' id='Parent'>
                                        //         <option value='Mother'>Mother</option>
                                        //         <option value='Father'>Father</option>
                                        //         <option value='None'>None</option>
                                        //         </select>";
                                        //     }
                                        // } else {
                                        //     echo   "<select name='Parent' id='Parent'>
                                        //     <option value='Mother'>Mother</option>
                                        //     <option value='Father'>Father</option>
                                        //     <option value='None'>None</option>
                                        //     </select>";
                                        // }
                                         ?>
                                        <input type="checkbox" id="mother" name="mother" value="mother">
                                        <label for="mother">Mother</label>
                                        <input type="checkbox" id="father" name="father" value="father">
                                        <label for="father">Father</label>
                                        <input type="checkbox" id="none" name="none" value="none">
                                        <label for="none">None</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Religion</label>
                                        <input type="text" id="Religion" name="Religion" class="form-control" placeholder="Christian, Muslim...." required >
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Occupation</label>
                                    <input type="text" class="form-control" placeholder="Country" value="Australia">
                                </div>
                            </div> -->
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Education Level</label>
                                        <input type="text" id="Education" name="Education" class="form-control" placeholder="High School, Tertiary...." required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Nearest Clinic</label>
                                        <input type="text" class="form-control" name="nearest_clinic" placeholder="Hhelehhele Clinc, etc...">
                                    </div>
                                </div>
                                <!-- <div class="col-md-4 px-1">
                                <div class="form-group">
                                    <label>Occupation</label>
                                    <input type="text" class="form-control" placeholder="Country" value="Australia">
                                </div>
                            </div> -->
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Nearest School</label>
                                        <input type="text" class="form-control"  name="nearest_school" placeholder="Simunye High School, etc....">
                                    </div>
                                </div>
                            </div>
                           
                            <div class="row">
                                <div class="update ml-auto mr-auto">
                                    <button type="submit" name="updatebtn" id="updatebtn" class="btn btn-primary btn-round">Create Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>

</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bps" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="rr" name="rr">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="NursesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                                echo $nstart; ?>" hidden="true">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>