<?php
session_start();

if(isset($_POST['logout_btn']) || isset($_REQUEST['logout_btn']))
{
    session_destroy();
    unset($_SESSION['username']);
    header('Location: login.php');
}

?>