<?php

require "includes/config.php";
require "includes/hts.php";
include('includes/header.php');
include('includes/htsnavbar.php');
?>

<div class="container-fluid">
    <?php
    // if (isset($_REQUEST['searchit'])) {
    //     $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
    //     $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
    //     $searchq = $_REQUEST['searchit'];
    //     $query = "SELECT * FROM clients WHERE " . $_REQUEST['criteria'] . " LIKE '%$searchq%'  ORDER BY `clients`.`Surname` ASC limit $start, 1";
    //     $query_run = mysqli_query($conn, $query);
    // } else
    if (isset($_REQUEST['emr_no'])) {
        $emr_no = $_REQUEST['emr_no'];
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        if (isset($_REQUEST['eject'])) {
            $query_Q = "UPDATE `queue` SET `status` = 0 WHERE `queue`.`emr_no` = $emr_no ";
            $query_Qrun = mysqli_query($conn, $query_Q);
        } else {
            $query_Q = "UPDATE `queue` SET `status` = " . $_SESSION["userid"] . " WHERE `queue`.`emr_no` = $emr_no AND status = 0";
            $query_Qrun = mysqli_query($conn, $query_Q);
        }
        $query = "SELECT * FROM clients , `queue` WHERE `queue`.`emr_no` = `clients`.`emr_no` AND `queue`.`department` = 'Hts' AND `queue`.`emr_no` = $emr_no  ORDER BY `queue`.`priority` DESC , `queue`.`date_enqueued`  ASC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    

        $row = mysqli_fetch_assoc($query_run);

        if ($row["status"] == $_SESSION['userid']) {
        //$row = mysqli_fetch_assoc($query_run);
        $emr_no1 = $row['emr_no'];
        $complaint = $row['reason'];
        if ($row['priority'] == 1) {
            $priority = "Company Employee";
        } else if ($row['priority'] == 2) {
            $priority = "Emergency";
        } else {
            $priority = "Regular";
        };


    ?>
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Patient Profile</h1>
    <a href="hts.php?emr_no=<?php echo $row['emr_no']."&eject=1"; ?>"  class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Eject</a>
  </div>

        <div class="row">

            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <!-- <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="..."> -->
                                <h5 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h5>
                            </a>
                            <p class="description text-center">
                                EMR #: <?php echo $row['emr_no']; ?>
                            </p>
                        </div>

                    </div>
                    <div class="card-footer">
                        <?php
                        $emr1 = $row['emr_no'];
                        $queryvitals1 = "SELECT * FROM vitals WHERE emr_no = $emr1 ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                        $query_run_vitals1 = mysqli_query($conn, $queryvitals1);
                        if (mysqli_num_rows($query_run_vitals1) > 0) {
                            $row_vitals1 = mysqli_fetch_assoc($query_run_vitals1);
                            $timefromdatabase = strtotime($row_vitals1['date_taken']);

                            $dif = time() - $timefromdatabase;
                            if ($dif < 86400) {
                        ?>

                                <h5 class="title text-center">Vitals (<?php echo $row_vitals1['date_taken']; ?>)</h5>
                        <?php
                            } else {
                                echo "<h5 class='title text-center'>Vitals </h5>";
                            }
                        } else {
                            echo "<h5 class='title text-center'>Vitals </h5>";
                        }

                        ?>
                        <hr>
                        <div class="button-container">
                            <?php
                            $emr = $row['emr_no'];
                            $weight = 1;
                            $temparature = 1;
                            $BP = 1;
                            $height = 1;
                            $queryvitals = "SELECT * FROM vitals WHERE emr_no = $emr ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                            $query_run_vitals = mysqli_query($conn, $queryvitals);
                            if (mysqli_num_rows($query_run_vitals) > 0) {
                                $row_vitals = mysqli_fetch_assoc($query_run_vitals);
                                $timefromdatabase = strtotime($row_vitals['date_taken']);

                                $dif = time() - $timefromdatabase;
                                if ($dif < 86400) {
                            ?>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['wt'];
                                                                    $weight = $row_vitals['temp']; ?>Kgs
                                                <br>
                                                <small>Weight</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo $row_vitals['temp'];
                                                                        $temparature = $row_vitals['temp']; ?> &#8451;
                                                <br>
                                                <small>Temparature</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6><?php echo $row_vitals['bp_sys']; ?> / <?php echo $row_vitals['bp_dia'];
                                                                                        $BP = $row_vitals['bp_sys'] . "/" . $row_vitals['bp_dia']; ?>
                                                <br>
                                                <small>BP</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['height'];
                                                                    $height = $row_vitals['height']; ?> cm
                                                <br>
                                                <small>Height</small>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['pulse']; ?>
                                                <br>
                                                <small>Pulse</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo $row_vitals['rr']; ?>
                                                <br>
                                                <small>RR</small>
                                            </h6>
                                        </div>
                                    </div>
                            <?php
                                } else {
                                    echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                                }
                            } else {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                            }

                            ?>
                            <?php
                            $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                            $query3 = "SELECT * FROM art_initiation_visit  WHERE emr_no = $emr_no1";
                            $query_run3 = mysqli_query($conn, $query3);
                            if (mysqli_num_rows($query_run3) > 0) {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PillCountModal'>Pill Adherence</button>";
                            }
                            ?>


                        </div>
                        <hr>
                        <h5 class="title text-center">Complaint: <?php echo $priority; ?> </h5>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 mr-auto">
                                <p>
                                    <?php echo $complaint; ?>
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Forward to Department</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#LabModal'>Lab</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#NurseModal'>Nurse</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#DismissModal'>Dismiss</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PharmModal'>Pharmacy</button>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-8"><?php
                if (isset($_POST['art_init'])) {

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query3 = "SELECT * FROM art_initiation_visit  WHERE emr_no = $emr_no1";
                    $query_run3 = mysqli_query($conn, $query3);
                    if (mysqli_num_rows($query_run3) > 0) {
                        echo "<h2>Art Initaiated</h2>";
                    } else include('includes/artinit.php');
                } else if (isset($_POST['confirm_register'])) {
                    include('includes/confirmation_register.php');
                } else if (isset($_POST['index_register'])) {
                    include('includes/index_register.php');
                } else if (isset($_POST['hts_register'])) {
                    include('includes/hts_register.php');
                } else if (isset($_POST['link_register'])) {
                    include('includes/linkage_register.php');
                }
            }
            else if (!isset($_REQUEST['eject'])) {?><input type='hidden' id='mybutton'  data-toggle='modal' data-target='#EmptyModal'/>
        
                <?php }
                ?>
            </div>

        </div>


</div>
<?php

    } else {
        echo "No records found";
    }

?>

</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bps" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="rr" name="rr">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="LabModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Lab Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="hts.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Lab" id="Lab_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="NurseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="hts.php"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="PharmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Pharmacy Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Prescription</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="hts.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Pharm" id="Pharm_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="EmptyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Client Occupied</h5>

                <!-- <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> -->
            </div>
            <div class="modal-body"><p>This client is currently being attended to</p>.</div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button> -->

                <form action="hts.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Continue</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="DismissModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Dismiss Home</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Dismiss</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="hts.php"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="dismiss" id="dismiss" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>