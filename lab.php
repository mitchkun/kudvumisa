<?php

require "includes/config.php";
require "includes/lab.php";
include('includes/header.php');
include('includes/labnavbar.php');
?>

<div class="container-fluid">
    <?php
    // if (isset($_REQUEST['searchit'])) {
    //     $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
    //     $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
    //     $searchq = $_REQUEST['searchit'];
    //     $query = "SELECT * FROM clients WHERE " . $_REQUEST['criteria'] . " LIKE '%$searchq%'  ORDER BY `clients`.`Surname` ASC limit $start, 1";
    //     $query_run = mysqli_query($conn, $query);
    // } else
    if (isset($_REQUEST['emr_no'])) {
        $emr_no = $_REQUEST['emr_no'];
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query = "SELECT * FROM clients , `queue` WHERE `queue`.`emr_no` = `clients`.`emr_no` AND `queue`.`department` = 'Lab' AND `queue`.`emr_no` = $emr_no  ORDER BY `queue`.`priority` DESC , `queue`.`date_enqueued`  ASC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    } else {
        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        $query = "SELECT * FROM clients , `queue` WHERE `queue`.`emr_no` = `clients`.`emr_no` AND `queue`.`department` = 'Lab' ORDER BY `queue`.`priority` DESC , `queue`.`date_enqueued`  ASC limit $start, 1";
        $query_run = mysqli_query($conn, $query);
    }


    if (mysqli_num_rows($query_run) > 0) {
        $row = mysqli_fetch_assoc($query_run);
        $emr_no1 = $row['emr_no'];
        $complaint = $row['reason'];
        if ($row['priority'] == 1) {
            $priority = "Company Employee";
        } else if ($row['priority'] == 2) {
            $priority = "Emergency";
        } else {
            $priority = "Regular";
        };


    ?>

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Client Profile</h1>
            <a href="lab.php?start=<?php if (isset($_REQUEST['searchit'])) {
                                        echo ($start == 0) ? 0 : $start - 1 . "&searchit=" . $_REQUEST['searchit'] . "&criteria=" . $_REQUEST['criteria'];
                                    } else {
                                        echo ($start == 0) ? 0 : $start - 1;
                                    } ?>" class="previous">&laquo; Previous</a>
            <a href="lab.php?<?php if (isset($_REQUEST['searchit'])) {
                                    $start++;
                                    echo "searchit=" . $_REQUEST['searchit'] . "&criteria=" . $_REQUEST['criteria'] . "&start=" . $start;
                                } else {
                                    $start++;
                                    echo "start=" . $start;
                                } ?>" class="next">Next &raquo;</a>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="card card-user">
                    <div class="card-body">
                        <div class="author">
                            <a href="#">
                                <!-- <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="..."> -->
                                <h5 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h5>
                            </a>
                            <p class="description text-center">
                                EMR #: <?php echo $row['emr_no']; ?>
                            </p>
                        </div>

                    </div>
                    <div class="card-footer">
                        <?php
                        $emr1 = $row['emr_no'];
                        $queryvitals1 = "SELECT * FROM vitals WHERE emr_no = $emr1 ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                        $query_run_vitals1 = mysqli_query($conn, $queryvitals1);
                        if (mysqli_num_rows($query_run_vitals1) > 0) {
                            $row_vitals1 = mysqli_fetch_assoc($query_run_vitals1);
                            $timefromdatabase = strtotime($row_vitals1['date_taken']);

                            $dif = time() - $timefromdatabase;
                            if ($dif < 86400) {
                        ?>

                                <h5 class="title text-center">Vitals (<?php echo $row_vitals1['date_taken']; ?>)</h5>
                        <?php
                            } else {
                                echo "<h5 class='title text-center'>Vitals </h5>";
                            }
                        } else {
                            echo "<h5 class='title text-center'>Vitals </h5>";
                        }

                        ?>
                        <hr>
                        <div class="button-container">
                            <?php
                            $emr = $row['emr_no'];
                            $weight = 1;
                            $temparature = 1;
                            $BP = 1;
                            $height = 1;
                            $queryvitals = "SELECT * FROM vitals WHERE emr_no = $emr ORDER BY `vitals`.`date_taken` DESC  LIMIT 0, 1";
                            $query_run_vitals = mysqli_query($conn, $queryvitals);
                            if (mysqli_num_rows($query_run_vitals) > 0) {
                                $row_vitals = mysqli_fetch_assoc($query_run_vitals);
                                $timefromdatabase = strtotime($row_vitals['date_taken']);

                                $dif = time() - $timefromdatabase;
                                if ($dif < 86400) {
                            ?>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['wt'];
                                                                    $weight = $row_vitals['temp']; ?>Kgs
                                                <br>
                                                <small>Weight</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo $row_vitals['temp'];
                                                                        $temparature = $row_vitals['temp']; ?> &#8451;
                                                <br>
                                                <small>Temparature</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6><?php echo $row_vitals['bp_sys']; ?> / <?php echo $row_vitals['bp_dia'];
                                                                                        $BP = $row_vitals['bp_sys'] . "/" . $row_vitals['bp_dia']; ?>
                                                <br>
                                                <small>BP</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['height'];
                                                                    $height = $row_vitals['height']; ?> cm
                                                <br>
                                                <small>Height</small>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-info"><?php echo $row_vitals['pulse']; ?>
                                                <br>
                                                <small>Pulse</small>
                                            </h6>
                                        </div>
                                        <div class="col-lg-3 mr-auto">
                                            <h6 class="text-warning"><?php echo $row_vitals['rr']; ?>
                                                <br>
                                                <small>RR</small>
                                            </h6>
                                        </div>
                                    </div>
                            <?php
                                } else {
                                    echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                                }
                            } else {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#VitalsModal'>Take Vitals</button>";
                            }

                            ?>
                            <?php
                            $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                            $query3 = "SELECT * FROM art_initiation_visit  WHERE emr_no = $emr_no1";
                            $query_run3 = mysqli_query($conn, $query3);
                            if (mysqli_num_rows($query_run3) > 0) {
                                echo "<button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#PillCountModal'>Pill Adherence</button>";
                            }
                            ?>


                        </div>
                        <hr>
                        <h5 class="title text-center">Complaint: <?php echo $priority; ?> </h5>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 mr-auto">
                                <p>
                                    <?php echo $complaint; ?>
                                </p>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Forward to Department</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#NursesModal'>Nurse</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#HtsModal'>HTS</button>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <button type='submit' class='btn btn-primary btn-round' data-toggle='modal' data-target='#DismissModal'>Dismiss</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <?php

            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo '<h2>' . $_SESSION['success'] . '</h2>';
                unset($_SESSION['success']);
            }

            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo '<h2>' . $_SESSION['status'] . '</h2>';
                unset($_SESSION['status']);
            }


            ?>
            <?php
             $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
            
            if (isset($_POST['cd'])) {
                $query3 = "SELECT * FROM cd4_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/cd4test.php');
            } else if (isset($_POST['vl'])) {
                $query3 = "SELECT * FROM ccf_viral_load_results  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/vltest.php');
            }
            else if (isset($_POST['art'])) {
                $query3 = "SELECT * FROM ccf_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/arttest.php');
            }
            else if (isset($_POST['preg'])) {
                $query3 = "SELECT * FROM preg_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/pregtest.php');
            }
            else if (isset($_POST['tb'])) {
                $query3 = "SELECT * FROM tb_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/tbtest.php');
            }
            else if (isset($_POST['other'])) {
                // $query3 = "SELECT * FROM other_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                // $query_run3 = mysqli_query($conn, $query3);
                // if (mysqli_num_rows($query_run3) > 0) {
                //     echo "<h2>Pending Results</h2>";
                // } else
                include('includes/othertest.php');
            }
            else if (isset($_POST['hiv'])) {
                $query3 = "SELECT * FROM hiv_lab_test  WHERE emr_no = $emr AND `status` = 'Pending'";
                $query_run3 = mysqli_query($conn, $query3);
                if (mysqli_num_rows($query_run3) > 0) {
                    echo "<h2>Pending Results</h2>";
                } else
                include('includes/hivtest.php');
            }
            ?>
        </div>


</div>
<?php

    } else {
        echo "No records found";
    }

?>
</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bps" id="pulse" name="pulse"> bps
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="rr" name="rr">
                                <input type="hidden" name="destination" value="lab.php"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="HtsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Hts Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="lab.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Hts" id="Hts_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="NursesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="lab.php"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="DismissModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Dismiss Home</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Dismiss</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                                                        <input type="hidden" name="destination" value="lab.php"/>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="dismiss" id="dismiss" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>