<?php

require "includes/config.php";
require "includes/nurse.php";
include('includes/header.php');
include('includes/arthistnavbar.php');
?>

<div class="container-fluid">
    <?php
    // if (isset($_REQUEST['searchit'])) {
    //     $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
    //     $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
    //     $searchq = $_REQUEST['searchit'];
    //     $query = "SELECT * FROM clients WHERE " . $_REQUEST['criteria'] . " LIKE '%$searchq%'  ORDER BY `clients`.`Surname` ASC limit $start, 1";
    //     $query_run = mysqli_query($conn, $query);
    // } else
    if (isset($_REQUEST['emr_no'])) {
        $emr_no = $_REQUEST['emr_no'];

        $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        if (isset($_REQUEST['eject'])) {
            $query_Q = "UPDATE `queue` SET `status` = 0 WHERE `queue`.`emr_no` = $emr_no ";
            $query_Qrun = mysqli_query($conn, $query_Q);
        } else {
            $query_Q = "UPDATE `queue` SET `status` = " . $_SESSION["userid"] . " WHERE `queue`.`emr_no` = $emr_no AND status = 0";
            $query_Qrun = mysqli_query($conn, $query_Q);
        }

        $query = "SELECT * FROM clients , `queue` WHERE `queue`.`emr_no` = `clients`.`emr_no` AND `queue`.`department` = 'Nurse' AND `queue`.`emr_no` = $emr_no  limit $start, 1";
        $query_run = mysqli_query($conn, $query);

        // } else {
        //     $start = !empty($_REQUEST['start']) ? $_REQUEST['start'] : 0;
        //     $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
        //     $query = "SELECT * FROM clients , `queue` WHERE `queue`.`emr_no` = `clients`.`emr_no` AND `queue`.`department` = 'Nurse' ORDER BY `queue`.`priority` DESC , `queue`.`date_enqueued`  ASC limit $start, 1";
        //     $query_run = mysqli_query($conn, $query);
        // }
        $row = mysqli_fetch_assoc($query_run);

        if ($row["status"] == $_SESSION['userid']) { //mysqli_num_rows($query_run) > 0) {
            //$row = mysqli_fetch_assoc($query_run);
            $emr_no1 = $row['emr_no'];
            $complaint = $row['reason'];
            if ($row['priority'] == 1) {
                $priority = "Company Employee";
            } else if ($row['priority'] == 2) {
                $priority = "Emergency";
            } else {
                $priority = "Regular";
            };


    ?>

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Patient Profile</h1>
                <a href="nurse.php?emr_no=<?php echo $row['emr_no'] . "&eject=1"; ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Eject</a>
            </div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <div class="card card-user">
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <!-- <img class="avatar border-gray" src="img/scan.gif" style="height: 124px; border: 1px solid #FFFFFF; position: relative;  display: block; margin-left: auto; margin-right: auto;" alt="..."> -->
                                    <h3 class="title text-center"><?php echo $row['Name'] . " " . $row['Surname']; ?></h3>
                                </a>
                                <h3 class="description text-center">
                                    EMR #: <?php echo $row['emr_no']; ?>
                                </h3>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="col-md-12">

                    <div class="card card-user" style="margin-top: 20px">
                        <div class="card-header">
                            <?php

                            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                                echo '<h2>' . $_SESSION['success'] . '</h2>';
                                unset($_SESSION['success']);
                            }

                            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                                echo '<h2>' . $_SESSION['status'] . '</h2>';
                                unset($_SESSION['status']);
                            }
                            //$emr_no = $_REQUEST['emr_no'];




                            ?>
                            <h1 class="card-title">CHRONIC CARE FILE HISTORY</h1>
                        </div>
                        <div class="card-body">

                            <div class="container-fluid">

                                <!-- Page Heading -->

                                <div class="table-wrapper">
                                    <!-- start table -->
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                            <!-- start headings -->
                                            <!-- heading row 1 -->

                                            <tr class="heading">
                                                <th rowspan="4">Visit Date</th>
                                                <th>Weight(kg)</th>
                                                <th>Nutritional Status</th>
                                                <th>WHO Stage</th>
                                                <th>Haemoglobin</th>
                                                <th>TB Screen</th>
                                                <th>Pregn. EDD</th>
                                                <th rowspan="4">Adherence(%)</th>
                                                <th>Comments</th>
                                                <th></th>

                                            </tr>

                                            <!-- heading row 2 -->

                                            <tr class="heading">
                                                <th>Height(cm)</th>
                                                <th>For Food Supplement</th>
                                                <th>Function Status</th>
                                                <th>HB</th>
                                                <th>Cervical Cancer Screening</th>
                                                <th>CTX</th>
                                                <th>Next Visit</th>
                                                <th>Sign</th>
                                            </tr>

                                            <!-- heading row 3 -->

                                            <tr class="heading">
                                                <th>BMI</th>
                                                <th>BP(mmHg)</th>
                                                <th>Cd4 Count(%)</th>
                                                <th>ALT</th>
                                                <th>Lactaction</th>
                                                <th>IPT</th>
                                            </tr>

                                            <!-- heading row 4 -->

                                            <tr class="heading">
                                                <th>MUAC(cm)</th>
                                                <th>T &#8451</th>
                                                <th>FP Use</th>
                                                <th>Creatinine</th>
                                                <th>STI Screening</th>
                                                <th>ANTI TB Rx</th>
                                            </tr>
                                        </thead>

                                        <!-- End headings -->

                                        <!-- Begin data rows -->
                                        <tbody>
                                            <?php
                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query = "SELECT * FROM `art_follow_up_visits` where `emr_no` = $emr_no1";
                    $query_run = mysqli_query($conn, $query);
                    if (mysqli_num_rows($query_run) > 0) {
                        while ($row = mysqli_fetch_assoc($query_run)) {
                                            ?>
                                            <!-- row 1 -->
                                            <tr class="data table-striped">
                                                <td rowspan="4"><?php echo $row['visit_date']; $visitdate = $row['visit_date']; ?></td>
                                                <td><?php echo $row['weight'] ?> kgs</td>
                                                <td><?php echo $row['nutritional_status'] ?></td>
                                                <td><?php echo $row['who_stage'] ?></td>
                                                <td><?php echo $row['haemoglobin'] ?></td>
                                                <td><?php echo $row['tb_screen'] ?></td>
                                                <td><?php echo $row['pregnancy_edd'] ?></td> <!-- Pregn. EDD -->
                                                <td rowspan="4"><?php $adherenceid = $row['adherence_id'];
                                                $query1 = "SELECT * FROM `adherence_pill_count` WHERE `apc_id` = $adherenceid " ;
                                                $query_run1 = mysqli_query($conn, $query1);
                                                if (mysqli_num_rows($query_run1) > 0) {
                                                    $row1 = mysqli_fetch_assoc($query_run1);
                                                    echo $row1['adherence_percentage'];
                                              } ?></td>
                                                <td><?php echo $row['next_visit_date'] ?></td>
                                                <td><?php echo $row['attending_id'] ?></td> <!-- Sign -->
                                            </tr>

                                            <tr class="data table-striped">
                                                <td><?php echo $row['height'] ?></td>
                                                <td><?php echo $row['food_supplement'] ?></td>
                                                <td><?php echo $row['functional_status'] ?></td>
                                                <td><?php echo $row['HB'] ?></td>
                                                <td><?php echo $row['HB'] ?></td>
                                                <td>Y</td>
                                            </tr>

                                            <tr class="data">
                                                <td>1.3</td>
                                                <td>140/90</td>
                                                <td>600</td>
                                                <td>7</td>
                                                <td>Y</td>
                                                <td>Y</td>
                                            </tr>

                                            <tr class="data">
                                                <td>100</td>
                                                <td>2</td>
                                                <td>5</td>
                                                <td>74.6</td>
                                                <td>Y</td>
                                                <td>N</td>
                                            </tr>
                                            <?php
                        }
                    }else {
                        echo "<tr>No records found</tr>";
                    }
                                            ?>

                                            <!-- row 2 -->
                                            

                                            <!-- End data rows -->
                                        </tbody>
                                    </table>
                                </div>

                                <!-- End table -->

                            </div>


                        </div>
                    </div>

                <?php
            } else if (!isset($_REQUEST['eject'])) { ?><input type='hidden' id='mybutton' data-toggle='modal' data-target='#EmptyModal' />

                <?php }
                ?>
                </div>
            </div>


</div>
<?php

    } else {
        echo "<a href='nurse.php' >No records found</a>";
    }

?>
</div>
<div class="modal fade" id="ScanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">UID</h5>

                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"><img class="img-profile" src="img\scan.gif" style="height: 150px;  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 50%;" alt="">.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>

                <form action="search.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Scan</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="EmptyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Client Occupied</h5>

                <!-- <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> -->
            </div>
            <div class="modal-body">
                <p>This client is currently being attended to</p>.
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button> -->

                <form action="arthistory.php" method="POST">

                    <button type="submit" name="logout_btn" class="btn btn-primary">Continue</button>

                </form>


            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="VitalsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="vitals.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Vitals</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Weight</label>
                                <input type="number" step="any" class="form-control" placeholder=".. Kgs" id="weight" name="weight"> kgs
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>Height</label>
                                <input type="number" step="any" class="form-control" placeholder="... cm" id="height" name="height"> cm
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>BP</label>
                                <input type="number" step="any" class="form-control" placeholder="Systolic" id="systolic" name="systolic">
                                <input type="number" step="any" class="form-control" placeholder="Diastolic" id="diastolic" name="diastolic">
                            </div>
                        </div>
                        <div class="col-md-3 pl-1">
                            <div class="form-group">
                                <label>Temperature</label>
                                <input type="number" step="any" class="form-control" placeholder="... &#8451;" id="temperature" name="temperature">&#8451;
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 pr-1">
                            <div class="form-group">
                                <label>Pulse</label>
                                <input type="number" step="any" class="form-control" placeholder=".. bpm" id="pulse" name="pulse"> bpm
                            </div>
                        </div>
                        <div class="col-md-3 px-1">
                            <div class="form-group">
                                <label>RR</label>
                                <input type="number" step="any" class="form-control" placeholder="... bpm" id="rr" name="rr"> bpm
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="vitals_save" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="NursesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Nurses Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Nurse" id="vitals_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="PillCountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="functions/pilladherence.php" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Pill Adherence</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php

                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                    $query4 = "SELECT * FROM pill_adherence WHERE emr_no = $emr_no1 ORDER BY pa_id DESC LIMIT 0,2";
                    $query_run4 = mysqli_query($conn, $query4);
                    $visit = "";
                    $ak = "";
                    $arv = "";
                    $dd = "";
                    $remaining = "";
                    $dlv = "";
                    $tth = "";
                    $rt = "";
                    $tslv = "";
                    $rnt = "";
                    $at = "";
                    $tslv = "";
                    $tdp = "";
                    $tslv = "";
                    $dslr = "";
                    $et = "";
                    $padherence = "";
                    $sadherence = "";
                    $amt_remaining = "";
                    if (mysqli_num_rows($query_run4) > 0) {
                        while ($row4 = mysqli_fetch_assoc($query_run4)) {
                            $visit .= "<td id='visit_date' name='visit_date' >" . $row4['visit_date'] . "</td>";
                            $ak .= "<td>" . $row4['appointment_keeping'] . "</td>";
                            $arv .= "<td>" . $row4['arv_regimen'] . "</td>";
                            $dd .= "<td>" . $row4['daily_dosing'] . "</td>";
                            $amt_remaining = $row4['ammount_remaining_last_refill'];
                            $remaining .= "<td>" . $row4['ammount_remaining_last_refill'] . "</td>";
                            $dlv .= "<td>" . $row4['dispensed_last_visit'] . "</td>";
                            $tth .= "<td>" . $row4['total_taken_home_last_visit'] . "</td>";
                            $rt .= "<td>" . $row4['remaining_today'] . "</td>";
                            $tslv .= "<td>" . $row4['taken_since_last_visit'] . "</td>";
                            $rnt .= "<td>" . $row4['reported_not_taken'] . "</td>";
                            $at .= "<td>" . $row4['actual_taken'] . "</td>";
                            $tdp .= "<td>" . $row4['total_daily_prescribed'] . "</td>";
                            $dslr .= "<td>" . $row4['days_since_last_refill'] . "</td>";
                            $et .= "<td>" . $row4['expected_taken_since_last_refill'] . "</td>";
                            $padherence .= "<td>" . $row4['perc_adherence'] . "</td>";
                            $sadherence .= "<td>" . $row4['adherence'] . "</td>";
                        }
                    }


                    ?>
                    <table>
                        <tr>
                            <td>Visit Date</td>
                            <?php if ($visit != "") {
                                echo $visit;
                            } ?>
                            <td><input type="date" id="refill_date" name="refill_date" class="form-control" required /></td>
                        </tr>
                        <tr>
                            <td>Appointment Keeping</td>
                            <?php if ($ak != "") {
                                echo $ak;
                            }  ?>
                            <td><select name='app_keeping' id='app_keeping' style="width: 100%" class="form-control" required>
                                    <option value=''>Select</option>
                                    <option value="On Time">On Time</option>
                                    <option value="Missed Appointment">Missed Appointment</option>
                                    <option value="Defaulted">Defaulted</option>
                                    <option value="Early">Early</option>
                                </select></td>
                        </tr>
                        <tr>
                            <td>ARV Regimen</td>
                            <?php if ($arv != "") {
                                echo $arv;
                            } ?>
                            <td><input type="text" id="arv_regimen" name="arv_regimen" class="form-control" required />
                                <input type="hidden" name="destination" value="nurse.php" />
                                <input type="hidden" name="emr_no" value="<?php echo $emr; ?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Daily Dosing</td>
                            <?php if ($dd != "") {
                                echo $dd;
                            } ?>
                            <td><input type="text" id="daily_dosing" name="daily_dosing" class="form-control" required /></td>
                        </tr>
                        <tr>
                            <td>Remaining</td>
                            <?php if ($remaining != "") {
                                echo $remaining;
                            }  ?>
                            <td><input type="number" id="remaining" name="remaining" required disabled="" value="<?php echo $amt_remaining; ?>" class="form-control" />
                            </td>
                        </tr>
                        <tr>
                            <td>Dispensed Last Visit</td>
                            <?php if ($dlv != "") {
                                echo $dlv;
                            }  ?>
                            <td><input type="number" id="dlv" name="dlv" class="form-control" required onchange="calculatetth()" /></td>
                        </tr>
                        <tr>
                            <td>Total taken Home</td>
                            <?php if ($tth != "") {
                                echo $tth;
                            } ?>
                            <td><input type="number" id="tth" name="tth" value="" required class="form-control" /></td>
                        </tr>
                        <tr>
                            <td>Remaining Today</td>
                            <?php if ($rt != "") {
                                echo $rt;
                            } ?>
                            <td><input type="number" id="rt" name="rt" class="form-control" required onchange="calculatetslv()" /></td>
                        </tr>
                        <tr>
                            <td>Taken Since last visit</td>
                            <?php if ($tslv != "") {
                                echo $tslv;
                            } ?>
                            <td><input type="number" id="tslv" name="tslv" value="" required class="form-control" /></td>
                        </tr>
                        <tr>
                            <td>Reported not taken</td>
                            <?php if ($rnt != "") {
                                echo $rnt;
                            } ?>
                            <td><input type="number" id="rnt" name="rnt" class="form-control" required onchange="calculateat()" /></td>
                        </tr>
                        <tr>
                            <td>Actual taken</td>
                            <?php if ($at != "") {
                                echo $at;
                            } ?>
                            <td><input type="number" id="at" name="at" value="" required class="form-control" /></td>
                        </tr>
                        <tr>
                            <td>Total daily Prescribed</td>
                            <?php if ($tdp != "") {
                                echo $tdp;
                            }  ?>
                            <td><input type="number" id="tds" name="tds" value="" class="form-control" required onchange="calculatedays(this.value)" /></td>
                        </tr>
                        <tr>
                            <td>Days since last refill</td>
                            <?php if ($dslr != "") {
                                echo $dslr;
                            } ?>
                            <td><input type="text" id="dslr" name="dslr" value="" required class="form-control" /></td>
                        </tr>
                        <tr>
                            <td>Expected taken</td>
                            <?php if ($et != "") {
                                echo $et;
                            } ?>
                            <td><input type="text" id="et" name="et" value="" required class="form-control" /></td>
                        </tr>
                        <tr>
                            <td>% Adherence</td>
                            <?php if ($padherence != "") {
                                echo $padherence;
                            } ?>
                            <td><input type="text" id="padher" name="padher" required value="" class="form-control" /></td>
                        </tr>
                        </tr>
                        <tr>
                            <td>Adherence</td>
                            <?php if ($sadherence != "") {
                                echo $sadherence;
                            } ?>
                            <td><input type="text" id="adher" name="adher" required value="" class="form-control" /></td>
                        </tr>

                    </table>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="pills_save" id="pills_save" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="HtsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Hts Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Hts" id="Hts_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="LabsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Labs Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Visit</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='2'>
                                <label for='working'>Emergency</label>
                                <input type='radio' id='ambulating' name='priority' value='1'>
                                <label for='ambulating'>Company Employees</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Lab" id="Labs_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="PharmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Pharmacy Department</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Medication</label>
                                <select name='meds[]' id='meds' style="width: 100%" class="form-control" required>
                                    <option value=''>Select</option>
                                    <?php
                                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                                    $query = "SELECT * FROM `medicine_master` ";
                                    $query_run = mysqli_query($conn, $query);
                                    if (mysqli_num_rows($query_run) > 0) {
                                        while ($row = mysqli_fetch_assoc($query_run)) {
                                    ?>
                                            <option value="<?php echo $row['name'] . " " . $row['dose'] . " " . $row['mg_ml']; ?>"><?php echo $row['name'] . " " . $row['dose'] . " " . $row['mg_ml']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <!-- <option value="Good Shepherd">Good Shepherd</option>
                                    <option value="Lancet">Lancet</option>
                                    <option value="PIMA">PIMA</option>
                                    <option value="Amphath">Amphath</option>
                                    <option value="Other">Lancet</option> -->
                                </select>
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dose</label>
                                <input type="text" class="form-control" id="dose" name="dose[]">
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dosage Form</label>
                                <input type="text" class="form-control" id="dosage" name="dosage[]">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Medication</label>
                                <select name='meds[]' id='meds1' style="width: 100%" class="form-control">
                                    <option value=''>Select</option>
                                    <?php
                                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                                    $query = "SELECT * FROM `medicine_master` ";
                                    $query_run = mysqli_query($conn, $query);
                                    if (mysqli_num_rows($query_run) > 0) {
                                        while ($row = mysqli_fetch_assoc($query_run)) {
                                    ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo $row['name'] . " " . $row['dose'] . " " . $row['mg_ml']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <!-- <option value="Good Shepherd">Good Shepherd</option>
                                    <option value="Lancet">Lancet</option>
                                    <option value="PIMA">PIMA</option>
                                    <option value="Amphath">Amphath</option>
                                    <option value="Other">Lancet</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dose</label>
                                <input type="text" class="form-control" id="dose1" name="dose[]">
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dosage Form</label>
                                <input type="text" class="form-control" id="dosage1" name="dosage[]">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Medication</label>
                                <select name='meds[]' id='meds2' style="width: 100%" class="form-control">
                                    <option value=''>Select</option>
                                    <?php
                                    $conn = mysqli_connect("localhost:3308", "root", "", "kudvumisa");
                                    $query = "SELECT * FROM `medicine_master` ";
                                    $query_run = mysqli_query($conn, $query);
                                    if (mysqli_num_rows($query_run) > 0) {
                                        while ($row = mysqli_fetch_assoc($query_run)) {
                                    ?>
                                            <option value="<?php echo $row['name']; ?>"><?php echo $row['name'] . " " . $row['dose'] . " " . $row['mg_ml']; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                    <!-- <option value="Good Shepherd">Good Shepherd</option>
                                    <option value="Lancet">Lancet</option>
                                    <option value="PIMA">PIMA</option>
                                    <option value="Amphath">Amphath</option>
                                    <option value="Other">Lancet</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dose</label>
                                <input type="text" class="form-control" id="dose2" name="dose[]">
                            </div>
                        </div>
                        <div class="col-md-4 pr-1">
                            <div class="form-group">
                                <label>Dosage Form</label>
                                <input type="text" class="form-control" id="dosage2" name="dosage[]">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <h5>Priority</h5>
                                <input type='radio' id='working' name='priority' value='ART Medication' required>
                                <label for='working'>ART Medication</label>
                                <input type='radio' id='ambulating' name='priority' value='Family Planning' required>
                                <label for='ambulating'>Family Planning</label>
                                <input type='radio' id='opd' name='priority' value='OPD Medication' required>
                                <label for='opd'>OPD Medication</label>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="Pharm" id="Pharm_save" class="btn btn-primary">Send</button>




                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="DismissModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="queue.php?emr_no=<?php echo $emr; ?>" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Dismiss Home</h5>

                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-8 pr-1">
                            <div class="form-group">
                                <label>Reason for Dismiss</label>
                                <input type="text" class="form-control" id="reason" name="reason">
                                <input id="start" name="start" value="<?php $nstart = $start - 1;
                                                                        echo $nstart; ?>" hidden="true">
                                <input type="hidden" name="destination" value="nurse.php" />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>



                    <button type="submit" name="dismiss" id="dismiss" class="btn btn-primary">Save</button>




                </div>
            </form>
        </div>
    </div>
</div>

<?php
include('includes/scripts.php');
include('includes/footer.php');
?>